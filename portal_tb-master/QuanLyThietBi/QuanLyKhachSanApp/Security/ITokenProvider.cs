﻿using System;
namespace HVIT.Security
{
    public interface ITokenProvider
    {
        bool ValidateToken(ref TokenIdentity tokenIdentity);
        TokenIdentity GenerateToken(int userId, string username, string userAgent, string ip, long effectiveTime);
    }
}
