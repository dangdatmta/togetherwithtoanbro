namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DaiLy")]
    public partial class DaiLy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DaiLy()
        {
            NhomViewBC_DaiLy = new HashSet<NhomViewBC_DaiLy>();
            PermissionBireport_DaiLy = new HashSet<PermissionBireport_DaiLy>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        public string ten { get; set; }

        public int? tinhthanh_id { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }
        public TinhThanh TinhThanh { get; set; }
        public ICollection<NhomViewBC_DaiLy> NhomViewBC_DaiLy { get; set; }
        public ICollection<PermissionBireport_DaiLy> PermissionBireport_DaiLy { get; set; }
    }
}
