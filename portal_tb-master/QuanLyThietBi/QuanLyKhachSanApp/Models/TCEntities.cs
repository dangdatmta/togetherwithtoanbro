using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace QuanLyKhachSanApp.Models
{
    public partial class TCEntities : DbContext
    {
        public TCEntities()
            : base("name=TCEntities")
        {
        }
        public virtual DbSet<AccessTokens> AccessTokenss { get; set; }
        public virtual DbSet<BoPhan> BoPhans { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        public virtual DbSet<ConfigReport> ConfigReports { get; set; }
        public virtual DbSet<DaiLy> DaiLies { get; set; }
        public virtual DbSet<KhuVuc> KhuVucs { get; set; }
        public virtual DbSet<KyBaoCao> KyBaoCaos { get; set; }
        public virtual DbSet<NhomLoaiDuLieu> NhomLoaiDuLieux { get; set; }
        public virtual DbSet<NhomNSD> NhomNSDs { get; set; }
        public virtual DbSet<ChucNang> ChucNangs { get; set; }
        public virtual DbSet<NhomViewBC> NhomViewBCs { get; set; }
        public virtual DbSet<NhomViewBC_Color> NhomViewBC_Color { get; set; }
        public virtual DbSet<NhomViewBC_DaiLy> NhomViewBC_DaiLy { get; set; }
        public virtual DbSet<NhomViewBC_KhuVuc> NhomViewBC_KhuVuc { get; set; }
        public virtual DbSet<NhomViewBC_SanPham> NhomViewBC_SanPham { get; set; }
        public virtual DbSet<NhomViewBC_TinhThanh> NhomViewBC_TinhThanh { get; set; }
        public virtual DbSet<Permission_bireport> Permission_bireport { get; set; }
        public virtual DbSet<PermissionBireport_Color> PermissionBireport_Color { get; set; }
        public virtual DbSet<PermissionBireport_DaiLy> PermissionBireport_DaiLy { get; set; }
        public virtual DbSet<PermissionBireport_KhuVuc> PermissionBireport_KhuVuc { get; set; }
        public virtual DbSet<PermissionBireport_SanPham> PermissionBireport_SanPham { get; set; }
        public virtual DbSet<PermissionBireport_TinhThanh> PermissionBireport_TinhThanh { get; set; }
        public virtual DbSet<SanPham> SanPhams { get; set; }
        public virtual DbSet<TCKT_01> TCKT_01 { get; set; }
        public virtual DbSet<TCKT_01_TRASH> TCKT_01_TRASH { get; set; }
        public virtual DbSet<TinhThanh> TinhThanhs { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<User_BoPhan> User_BoPhan { get; set; }
        public virtual DbSet<User_NhomNSD> User_NhomNSD { get; set; }
        public virtual DbSet<NhomNSD_ChucNang> NhomNSD_ChucNang { get; set; }
        public virtual DbSet<NLDL_KyBC> NLDL_KyBCs { get; set; }
        public virtual DbSet<Validate_TCKT_01> Validate_TCKT01s { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BoPhan>()
                .HasMany(e => e.ConfigReports)
                .WithOptional(e => e.BoPhan)
                .HasForeignKey(e => e.bophan_id);

            modelBuilder.Entity<BoPhan>()
                .HasMany(e => e.NhomLoaiDuLieux)
                .WithOptional(e => e.BoPhan)
                .HasForeignKey(e => e.bophan_id);

            modelBuilder.Entity<BoPhan>()
                .HasMany(e => e.User_BoPhan)
                .WithOptional(e => e.BoPhan)
                .HasForeignKey(e => e.BoPhan_id);

            modelBuilder.Entity<Color>()
                .HasMany(e => e.NhomViewBC_Color)
                .WithOptional(e => e.Color)
                .HasForeignKey(e => e.color_id);

            modelBuilder.Entity<Color>()
                .HasMany(e => e.PermissionBireport_Color)
                .WithOptional(e => e.Color)
                .HasForeignKey(e => e.color_id);

            modelBuilder.Entity<ConfigReport>()
                .Property(e => e.tableurl_id)
                .IsUnicode(false);

            modelBuilder.Entity<DaiLy>()
                .HasMany(e => e.NhomViewBC_DaiLy)
                .WithOptional(e => e.DaiLy)
                .HasForeignKey(e => e.daily_id);

            modelBuilder.Entity<DaiLy>()
                .HasMany(e => e.PermissionBireport_DaiLy)
                .WithOptional(e => e.DaiLy)
                .HasForeignKey(e => e.daily_id);

            modelBuilder.Entity<KhuVuc>()
                .HasMany(e => e.NhomViewBC_KhuVuc)
                .WithOptional(e => e.KhuVuc)
                .HasForeignKey(e => e.khuvuc_id);

            modelBuilder.Entity<KhuVuc>()
                .HasMany(e => e.PermissionBireport_KhuVuc)
                .WithOptional(e => e.KhuVuc)
                .HasForeignKey(e => e.khuvuc_id);

            modelBuilder.Entity<KhuVuc>()
                .HasMany(e => e.TinhThanhs)
                .WithOptional(e => e.Khuvuc)
                .HasForeignKey(e => e.khuvuc_id);

            modelBuilder.Entity<KyBaoCao>()
                .HasMany(e => e.NLDL_KyBCs)
                .WithOptional(e => e.KyBaoCao)
                .HasForeignKey(e => e.kybc_id);

            modelBuilder.Entity<NhomLoaiDuLieu>()
                .HasMany(e => e.NLDL_KyBCs)
                .WithOptional(e => e.NhomLoaiDuLieu)
                .HasForeignKey(e => e.NLDL_id);

            modelBuilder.Entity<NhomLoaiDuLieu>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<NhomLoaiDuLieu>()
                .Property(e => e.filetemplate)
                .IsUnicode(false);

            modelBuilder.Entity<NhomLoaiDuLieu>()
                .Property(e => e.tenbang_sql)
                .IsUnicode(false);

            modelBuilder.Entity<NLDL_KyBC>()
                .HasMany(e => e.TCKT_01)
                .WithOptional(e => e.NLDL_KyBC)
                .HasForeignKey(e => e.nldl_kybc_id);

            modelBuilder.Entity<NhomNSD>()
                .HasMany(e => e.User_NhomNSD)
                .WithOptional(e => e.NhomNSD)
                .HasForeignKey(e => e.NhomNSD_id);

            modelBuilder.Entity<ChucNang>()
                .HasMany(e => e.User_ChucNang)
                .WithOptional(e => e.ChucNang)
                .HasForeignKey(e => e.chucnang_id);

            modelBuilder.Entity<NhomViewBC>()
                .HasMany(e => e.NhomViewBC_Color)
                .WithOptional(e => e.NhomViewBC)
                .HasForeignKey(e => e.nhomviewbc_id);

            modelBuilder.Entity<NhomViewBC>()
                .HasMany(e => e.NhomViewBC_DaiLy)
                .WithOptional(e => e.NhomViewBC)
                .HasForeignKey(e => e.nhomviewbc_id);

            modelBuilder.Entity<NhomViewBC>()
                .HasMany(e => e.NhomViewBC_KhuVuc)
                .WithOptional(e => e.NhomViewBC)
                .HasForeignKey(e => e.nhomviewbc_id);

            modelBuilder.Entity<NhomViewBC>()
                .HasMany(e => e.NhomViewBC_SanPham)
                .WithOptional(e => e.NhomViewBC)
                .HasForeignKey(e => e.nhomviewbc_id);

            modelBuilder.Entity<NhomViewBC>()
                .HasMany(e => e.NhomViewBC_TinhThanh)
                .WithOptional(e => e.NhomViewBC)
                .HasForeignKey(e => e.nhomviewbc_id);

            modelBuilder.Entity<Permission_bireport>()
                .HasMany(e => e.PermissionBireport_Color)
                .WithOptional(e => e.Permission_bireport)
                .HasForeignKey(e => e.permission_bireport_id);

            modelBuilder.Entity<Permission_bireport>()
                .HasMany(e => e.PermissionBireport_DaiLy)
                .WithOptional(e => e.Permission_bireport)
                .HasForeignKey(e => e.permission_bireport_id);

            modelBuilder.Entity<Permission_bireport>()
                .HasMany(e => e.PermissionBireport_KhuVuc)
                .WithOptional(e => e.Permission_bireport)
                .HasForeignKey(e => e.permission_bireport_id);

            modelBuilder.Entity<Permission_bireport>()
                .HasMany(e => e.PermissionBireport_SanPham)
                .WithOptional(e => e.Permission_bireport)
                .HasForeignKey(e => e.permission_bireport_id);

            modelBuilder.Entity<Permission_bireport>()
                .HasMany(e => e.PermissionBireport_TinhThanh)
                .WithRequired(e => e.Permission_bireport)
                .HasForeignKey(e => e.permission_bireport_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SanPham>()
                .HasMany(e => e.NhomViewBC_SanPham)
                .WithOptional(e => e.SanPham)
                .HasForeignKey(e => e.sanpham_id);

            modelBuilder.Entity<SanPham>()
                .HasMany(e => e.PermissionBireport_SanPham)
                .WithOptional(e => e.SanPham)
                .HasForeignKey(e => e.sanpham_id);

            modelBuilder.Entity<TinhThanh>()
                .HasMany(e => e.NhomViewBC_TinhThanh)
                .WithOptional(e => e.TinhThanh)
                .HasForeignKey(e => e.tinhthanh_id);

            modelBuilder.Entity<TinhThanh>()
                .HasMany(e => e.DaiLys)
                .WithOptional(e => e.TinhThanh)
                .HasForeignKey(e => e.tinhthanh_id);

            modelBuilder.Entity<TinhThanh>()
                .HasMany(e => e.PermissionBireport_TinhThanh)
                .WithOptional(e => e.TinhThanh)
                .HasForeignKey(e => e.tinhthanh_id);

            modelBuilder.Entity<User>()
                .Property(e => e.sdt)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.tendangnhap)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.matkhau)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.User_BoPhans)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.User_NhomNSDs)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.user_id);

            modelBuilder.Entity<NhomNSD>()
                .HasMany(e => e.NhomNSD_ChucNang)
                .WithOptional(e => e.NhomNSD)
                .HasForeignKey(e => e.nhomnsd_id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AccessTokenss)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.UserId);
        }
    }
}
