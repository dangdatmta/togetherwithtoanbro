namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("KyBaoCao")]
    public partial class KyBaoCao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KyBaoCao()
        {
            NLDL_KyBCs = new HashSet<NLDL_KyBC>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [StringLength(50)]
        public string ten { get; set; }

        public int? nam { get; set; }

        public int? quy { get; set; }

        public int? thang { get; set; }
        public int? ngay { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngaybatdau { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayketthuc { get; set; }

        [StringLength(500)]
        public string ghichu { get; set; }

        public int? trangthai { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }
        public int? loaiky { get; set; }
        public ICollection<NLDL_KyBC> NLDL_KyBCs { get; set; }
    }
}
