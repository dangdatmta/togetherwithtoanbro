namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class NhomViewBC_TinhThanh
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int? nhomviewbc_id { get; set; }

        public int? tinhthanh_id { get; set; }

        public NhomViewBC NhomViewBC { get; set; }

        public TinhThanh TinhThanh { get; set; }
    }
}
