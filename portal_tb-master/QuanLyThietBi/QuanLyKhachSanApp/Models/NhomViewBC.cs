namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NhomViewBC")]
    public partial class NhomViewBC
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NhomViewBC()
        {
            NhomViewBC_Color = new HashSet<NhomViewBC_Color>();
            NhomViewBC_DaiLy = new HashSet<NhomViewBC_DaiLy>();
            NhomViewBC_KhuVuc = new HashSet<NhomViewBC_KhuVuc>();
            NhomViewBC_SanPham = new HashSet<NhomViewBC_SanPham>();
            NhomViewBC_TinhThanh = new HashSet<NhomViewBC_TinhThanh>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        public string ten { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }

        public ICollection<NhomViewBC_Color> NhomViewBC_Color { get; set; }
        public ICollection<NhomViewBC_DaiLy> NhomViewBC_DaiLy { get; set; }
        public ICollection<NhomViewBC_KhuVuc> NhomViewBC_KhuVuc { get; set; }
        public ICollection<NhomViewBC_SanPham> NhomViewBC_SanPham { get; set; }
        public ICollection<NhomViewBC_TinhThanh> NhomViewBC_TinhThanh { get; set; }
    }
}
