namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[TCKT.01_TRASH]")]
    public partial class TCKT_01_TRASH
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        public string model { get; set; }

        public double? giatamtinh { get; set; }

        public int? version { get; set; }
    }
}
