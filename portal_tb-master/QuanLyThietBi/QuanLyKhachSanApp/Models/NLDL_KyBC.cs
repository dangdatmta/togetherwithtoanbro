﻿namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NLDL_KyBC")]
    public partial class NLDL_KyBC
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NLDL_KyBC()
        {
            TCKT_01 = new HashSet<TCKT_01>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int? NLDL_id { get; set; }

        public int? kybc_id { get; set; }
        public int? trangthai { get; set; }

        public NhomLoaiDuLieu NhomLoaiDuLieu { get; set; }

        public KyBaoCao KyBaoCao { get; set; }
        public ICollection<TCKT_01> TCKT_01 { get; set; }
    }
}
