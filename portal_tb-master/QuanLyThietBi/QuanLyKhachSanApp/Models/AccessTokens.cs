﻿namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccessTokens")]
    public partial class AccessTokens
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int AccessTokenId { get; set; }
        [StringLength(1000)]
        public string Token { get; set; }
        [StringLength(50)]
        public string UserName { get; set; }
        public DateTime? EffectiveTime { get; set; }

        [StringLength(1000)]
        public string UserAgent { get; set; }

        [StringLength(150)]
        public string IP { get; set; }
        public long? ExpiresIn { get; set; }
        public User User { get; set; }
        public int? UserId { get; set; }
    }
}
