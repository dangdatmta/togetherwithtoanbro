﻿namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[Validate_TCKT.01]")]
    public partial class Validate_TCKT_01
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public String model { get; set; }
        public String giatamtinh { get; set; }
        public int? ver { get; set; }
    }
}
