namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Permission-bireport")]
    public partial class Permission_bireport
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Permission_bireport()
        {
            PermissionBireport_Color = new HashSet<PermissionBireport_Color>();
            PermissionBireport_DaiLy = new HashSet<PermissionBireport_DaiLy>();
            PermissionBireport_KhuVuc = new HashSet<PermissionBireport_KhuVuc>();
            PermissionBireport_SanPham = new HashSet<PermissionBireport_SanPham>();
            PermissionBireport_TinhThanh = new HashSet<PermissionBireport_TinhThanh>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        public string ten { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }

        public ICollection<PermissionBireport_Color> PermissionBireport_Color { get; set; }
        public ICollection<PermissionBireport_DaiLy> PermissionBireport_DaiLy { get; set; }
        public ICollection<PermissionBireport_KhuVuc> PermissionBireport_KhuVuc { get; set; }
        public ICollection<PermissionBireport_SanPham> PermissionBireport_SanPham { get; set; }
        public ICollection<PermissionBireport_TinhThanh> PermissionBireport_TinhThanh { get; set; }
    }
}
