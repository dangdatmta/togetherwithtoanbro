namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConfigReport")]
    public partial class ConfigReport
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int? trangthai { get; set; }

        public int? thutu { get; set; }

        public int? bophan_id { get; set; }

        [StringLength(100)]
        public string text { get; set; }
        [StringLength(100)]
        public string link { get; set; }

        [StringLength(1000)]
        public string tableurl_id { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }

        public BoPhan BoPhan { get; set; }
    }
}
