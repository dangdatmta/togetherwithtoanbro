namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User_NhomNSD
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int? user_id { get; set; }

        public int? NhomNSD_id { get; set; }

        public NhomNSD NhomNSD { get; set; }

        public User User { get; set; }
    }
}
