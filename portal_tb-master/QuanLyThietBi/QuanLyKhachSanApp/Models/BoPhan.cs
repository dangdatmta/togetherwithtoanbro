namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BoPhan")]
    public partial class BoPhan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BoPhan()
        {
            ConfigReports = new HashSet<ConfigReport>();
            NhomLoaiDuLieux = new HashSet<NhomLoaiDuLieu>();
            User_BoPhan = new HashSet<User_BoPhan>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [StringLength(100)]
        public string code { get; set; }
        [StringLength(100)]
        public string text { get; set; }
        [StringLength(100)]
        public string link { get; set; }

        [StringLength(500)]
        public string ghichu { get; set; }

        public int? trangthai { get; set; }

        [StringLength(100)]
        public string nguoitao { get; set; }

        [StringLength(100)]
        public string nguoisua { get; set; }

        public DateTime? thoigiantao { get; set; }

        public DateTime? thoigiansua { get; set; }
        public int? stt { get; set; }
        public ICollection<ConfigReport> ConfigReports { get; set; }
        public ICollection<NhomLoaiDuLieu> NhomLoaiDuLieux { get; set; }
        public ICollection<User_BoPhan> User_BoPhan { get; set; }
    }
}
