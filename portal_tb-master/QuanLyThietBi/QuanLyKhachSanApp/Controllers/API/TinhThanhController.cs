﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/tinhthanh")]
    public class TinhThanhController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").ToListAsync();
                var tinhthanh = await db.TinhThanhs.ToListAsync();

                //var user = await db.Users.Include("UserRole").ToListAsync();
                if (tinhthanh == null)
                    return NotFound();
                return Ok(tinhthanh);
            }
        }
        [HttpGet, Route("{tinhthanhID}")]
        public async Task<IHttpActionResult> GetById(int tinhthanhID)
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").SingleOrDefaultAsync(o => o.Id == userId);
                var user = await db.TinhThanhs.SingleOrDefaultAsync(o => o.id == tinhthanhID);
                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] TinhThanh tinhthanh)
        {
            using (var db = new TCEntities())
            {
                List<string> listthinhthanh = await db.TinhThanhs.Select(x => x.ten).ToListAsync();
                foreach (var tinh in listthinhthanh)
                {
                    if (tinh == tinhthanh.ten)
                    {
                        return BadRequest("Tỉnh thành này đã tồn tại!!!");
                    }
                }
                db.TinhThanhs.Add(tinhthanh);
                await db.SaveChangesAsync();
            }
            return Ok(tinhthanh);
        }
        [HttpPut, Route("{tinhthanhID}")]
        public async Task<IHttpActionResult> Update(int tinhthanhID, [FromBody] TinhThanh tinhthanh)
        {
            if (tinhthanh.id != tinhthanhID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(tinhthanh).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.TinhThanhs.Count(o => o.id == tinhthanhID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(tinhthanh);
            }
        }
        [HttpDelete, Route("{tinhthanhID}")]
        public async Task<String> Delete(int tinhthanhID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var tinhthanh = await db.TinhThanhs.SingleOrDefaultAsync(o => o.id == tinhthanhID);
                var tinhthanh_daily = await db.DaiLies.Where(o => o.tinhthanh_id == tinhthanhID).ToListAsync();
                var tinhthanh_per_bi = await db.PermissionBireport_TinhThanh.Where(o => o.tinhthanh_id == tinhthanhID).ToListAsync();
                var tinhthanh_vbc = await db.NhomViewBC_TinhThanh.Where(o => o.tinhthanh_id == tinhthanhID).ToListAsync();
                if (tinhthanh == null)
                {
                    message = "Không tìm thấy Tỉnh thành";
                }
                else
                {
                    foreach (var UGR in tinhthanh_daily)
                    {
                        db.Entry(UGR).State = EntityState.Deleted;
                    }
                    foreach (var per in tinhthanh_per_bi)
                    {
                        db.Entry(per).State = EntityState.Deleted;
                    }
                    foreach (var vbc in tinhthanh_vbc)
                    {
                        db.Entry(vbc).State = EntityState.Deleted;
                    }
                    db.Entry(tinhthanh).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa tỉnh thành thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }

                return message;
            }
        }

        // GET: TinhThanh

    }
}