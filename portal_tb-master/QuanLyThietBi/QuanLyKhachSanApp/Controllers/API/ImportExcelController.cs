﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Net.Http;
using System.Web;
using QuanLyKhachSanApp.Models;
using System.IO;
using ExcelDataReader;
using System.Collections.Generic;
using System.Data;
using System.Net;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/excel")]
    public class ImportExcelController : BaseApiController
    {
        [HttpGet, Route("{nldl_id},{kybc_id}")]
        public async Task<IHttpActionResult> Get(int nldl_id, int kybc_id)
        {
            using (var db = new TCEntities())
            {
                //List<F_CSKH_05> bc = new List<F_CSKH_05>();
                /*List<int?> ver = await (from bc in db.F_CSKH_05
                                        select bc.version).Distinct().OrderByDescending(x=>x.Value).ToListAsync();*/

                //List<int?> ver = await db.F_CSKH_05.Select(o=>o.version).Distinct().OrderByDescending(x=>x.Value).ToListAsync();

                List<int?> stt = await db.TCKT_01.Where(m=>m.NLDL_KyBC.NLDL_id == nldl_id && m.NLDL_KyBC.kybc_id == kybc_id).Select(o => o.stt).Distinct().OrderByDescending(x => x.Value).ToListAsync();

                //var ver = await db.F_CSKH_05.ToListAsync();
                //List<int?> ver = await db.F_CSKH_05.Select(o => o.version).OrderByDescending().Distinct().ToListAsync();
                //bc = await db.F_CSKH_05.GroupBy(x=>x.version).Select(g=>g.FirstOrDefault()).ToListAsync();

                if (stt == null)
                    return NotFound();

                return Ok(stt);
            }
        }
        /*[HttpGet, Route("{version}")]
        public async Task<IHttpActionResult> GetByVersion(int version)
        {
            using (var db = new TCEntities())
            {
                var bc = await db.F_CSKH_05.Where(o => o.version == version).ToListAsync();
                //GroupBy(p=>p.CreateDate).ToListAsync();

                if (bc == null)
                    return NotFound();

                return Ok(bc);
            }
        }*/
        [HttpGet, Route("{stt},{nldl_id},{kybc_id}")]
        public async Task<IHttpActionResult> GetByStt(int stt, int nldl_id, int kybc_id)
        {
            using (var db = new TCEntities())
            {
                //var bc = await db.TCKT_01.Where(o => o.stt == stt).ToListAsync();
                var bc = await db.TCKT_01.Where(o => o.NLDL_KyBC.NLDL_id == nldl_id && o.NLDL_KyBC.kybc_id == kybc_id).Where(o => o.stt == stt).ToListAsync();
                if (bc == null)
                    return NotFound();

                return Ok(bc);
            }
        }
        /*        [HttpGet, Route("{version}")]
                public async Task<IHttpActionResult> GetByVersion()
                {
                    using (var db = new TCEntities())
                    {
                       *//* List<int?> listVer = await db.F_CSKH_05.Select(x => x.version).Distinct().ToListAsync();
                        List<F_CSKH_05> bc = new List<F_CSKH_05>();
                        List<F_CSKH_05> bcReal = new List<F_CSKH_05>();
                        foreach (var ver in listVer)
                        {
                            bc = await db.F_CSKH_05.Where(o => o.version == ver).ToListAsync();
                            bcReal.AddRange(bc);
                        }

                        //GroupBy(p=>p.CreateDate).ToListAsync();

                        if (bcReal == null)
                            return NotFound();

                        return Ok(bcReal);*//*
                    }
                }*/

        [HttpPost, Route("{nldl_id},{kybc_id}")]
        public string ExcelUpload(int nldl_id, int kybc_id)
        {
            string message = "";
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            using (var objEntity = new TCEntities())
            {
                if (httpRequest.Files.Count > 0)
                {
                    HttpPostedFile file = httpRequest.Files[0];
                    Stream stream = file.InputStream;

                    IExcelDataReader reader = null;

                    if (file.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        DataSet excelRecords = reader.AsDataSet();
                        reader.Close();

                        var verMax = objEntity.TCKT_01.Where(o => o.NLDL_KyBC.kybc_id == kybc_id && o.NLDL_KyBC.NLDL_id == nldl_id).Max(x => x.version);
                        if (verMax == null)
                        {
                            verMax = 0;
                        }
                        int? stt = objEntity.TCKT_01.Where(o=>o.NLDL_KyBC.kybc_id == kybc_id && o.NLDL_KyBC.NLDL_id == nldl_id).Max(x => x.stt);
                        if (stt == null)
                        {
                            stt = 0;
                        }
                        var finalRecords = excelRecords.Tables[0];
                        for (int i = 1; i < finalRecords.Rows.Count; i++)
                        {
                            TCKT_01 objUser = new TCKT_01();
                            objUser.header1 = finalRecords.Rows[0][0].ToString();
                            objUser.header2 = finalRecords.Rows[0][1].ToString();
                            objUser.model = finalRecords.Rows[i][0].ToString();
                            objUser.giatamtinh = double.Parse(finalRecords.Rows[i][1].ToString());
                            objUser.ngaytao = DateTime.Now;
                            //objUser.UpdateDate = DateTime.Parse(finalRecords.Rows[i][3].ToString());
                            //objUser.CreateUser = finalRecords.Rows[i][4].ToString();
                            //objUser.UpdateUser = finalRecords.Rows[i][5].ToString();
                            objUser.version = verMax + 1;
                            objUser.stt = stt + 1;
                            objUser.nldl_kybc_id = objEntity.NLDL_KyBCs.Where(x => x.NLDL_id == nldl_id && x.kybc_id == kybc_id).Select(o=>o.id).FirstOrDefault();
                            objEntity.TCKT_01.Add(objUser);
                        }
                        int output = objEntity.SaveChanges();
                        if (output > 0)
                        {
                            message = "Excel file has been successfully uploaded";
                        }
                        else
                        {
                            message = "Excel file uploaded has faild";
                        }
                    }
                    else if (file.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        DataSet excelRecords = reader.AsDataSet();
                        reader.Close();

                        var verMax = objEntity.TCKT_01.Where(o => o.NLDL_KyBC.kybc_id == kybc_id && o.NLDL_KyBC.NLDL_id == nldl_id).Max(x => x.version);
                        if (verMax == null)
                        {
                            verMax = 0;
                        }
                        int? stt = objEntity.TCKT_01.Where(o => o.NLDL_KyBC.kybc_id == kybc_id && o.NLDL_KyBC.NLDL_id == nldl_id).Max(x => x.stt);
                        if (stt == null)
                        {
                            stt = 0;
                        }
                        var finalRecords = excelRecords.Tables[0];
                        for (int i = 1; i < finalRecords.Rows.Count; i++)
                        {
                            TCKT_01 objUser = new TCKT_01();
                            objUser.header1 = finalRecords.Rows[0][0].ToString();
                            objUser.header2 = finalRecords.Rows[0][1].ToString();
                            objUser.model = finalRecords.Rows[i][0].ToString();
                            objUser.giatamtinh = double.Parse(finalRecords.Rows[i][1].ToString());
                            objUser.ngaytao = DateTime.Now;
                            //objUser.UpdateDate = DateTime.Parse(finalRecords.Rows[i][3].ToString());
                            //objUser.CreateUser = finalRecords.Rows[i][4].ToString();
                            //objUser.UpdateUser = finalRecords.Rows[i][5].ToString();
                            objUser.version = verMax + 1;
                            objUser.stt = stt + 1;
                            objUser.nldl_kybc_id = objEntity.NLDL_KyBCs.Where(x => x.NLDL_id == nldl_id && x.kybc_id == kybc_id).Select(o => o.id).FirstOrDefault();
                            objEntity.TCKT_01.Add(objUser);
                        }
                        int output = objEntity.SaveChanges();
                        if (output > 0)
                        {
                            message = "Excel file has been successfully uploaded";
                        }
                        else
                        {
                            message = "Excel file uploaded has faild";
                        }
                    }
                    else
                    {
                        message = "This file format is not supported";
                    }

                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            return message;
        }
        [HttpPost, Route("{version}")]
        public async Task<IHttpActionResult> ExcelOverride(int version)
        {
            using (var db = new TCEntities())
            {
                List<TCKT_01> listReportFollowVer = await db.TCKT_01.Where(x => x.stt == version).ToListAsync();
                TCKT_01_TRASH f_tckt01_trash = new TCKT_01_TRASH();
                var sttMax = db.TCKT_01.Max(x => x.stt);
                List<TCKT_01> listReportFollowVer1 = await db.TCKT_01.Where(x => x.stt == sttMax).ToListAsync();
                foreach (var reportFollowVer in listReportFollowVer1)
                {
                    reportFollowVer.stt = version;
                    db.Entry(reportFollowVer).State = EntityState.Modified;
                    db.SaveChanges();
                }

                foreach (var reportFollowVer in listReportFollowVer)
                {
                    f_tckt01_trash.model = reportFollowVer.model;
                    f_tckt01_trash.giatamtinh = reportFollowVer.giatamtinh; 
                    f_tckt01_trash.version = reportFollowVer.version;

                    db.TCKT_01_TRASH.Add(f_tckt01_trash);
                    reportFollowVer.stt = sttMax;
                    db.Entry(reportFollowVer).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Ok(f_tckt01_trash);
            }
        }


        [HttpPost, Route("validate")]
        public List<string> ValidateExcel()
        {
            List<string> listmessage = new List<string>();
            string message = "";
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            using (var objEntity = new TCEntities())
            {
                if (httpRequest.Files.Count > 0)
                {
                    HttpPostedFile file = httpRequest.Files[0];
                    Stream stream = file.InputStream;

                    IExcelDataReader reader = null;

                    if (file.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        DataSet excelRecords = reader.AsDataSet();
                        reader.Close();
                        int? verMax = objEntity.Validate_TCKT01s.Max(x => x.ver);
                        if (verMax == null)
                        {
                            verMax = 0;
                        }
                        var finalRecords = excelRecords.Tables[0];
                        for (int i = 1; i < finalRecords.Rows.Count; i++)
                        {
                            if (finalRecords.Rows[i][0].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 1 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][1].GetType() != typeof(Double))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 2 phải là kiểu số!!!";
                                listmessage.Add(message);
                            }

                            Validate_TCKT_01 objUser = new Validate_TCKT_01();
                            objUser.model = finalRecords.Rows[i][0].ToString();
                            objUser.giatamtinh = finalRecords.Rows[i][1].ToString();
                            objUser.ver = verMax + 1;
                            objEntity.Validate_TCKT01s.Add(objUser);
                            objEntity.SaveChanges();
                        }
/*                        int output = objEntity.SaveChanges();
                        if (output > 0)
                        {
                            message = "Excel file has been successfully uploaded";
                        }
                        else
                        {
                            message = "Excel file uploaded has faild";
                        }*/
                    }
                    else if (file.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        DataSet excelRecords = reader.AsDataSet();
                        reader.Close();
                        var finalRecords = excelRecords.Tables[0];
                        int? verMax = objEntity.Validate_TCKT01s.Max(x => x.ver);
                        if(verMax == null)
                        {
                            verMax = 0;
                        }
                        for (int i = 1; i < finalRecords.Rows.Count; i++)
                        {
                            if (finalRecords.Rows[i][0].GetType() != typeof(string))
                            {
                                message = "Dữ liệu ở hàng "+i+" cột 1 phải là kiểu chữ!!!";
                                listmessage.Add(message);
                            }
                            if (finalRecords.Rows[i][1].GetType() != typeof(Double))
                            {
                                message = "Dữ liệu ở hàng " + i + " cột 2 phải là kiểu double!!!";
                                listmessage.Add(message);
                            }
                            
                            Validate_TCKT_01 objUser = new Validate_TCKT_01();
                            objUser.model = finalRecords.Rows[i][0].ToString();
                            objUser.giatamtinh = finalRecords.Rows[i][1].ToString();
                            objUser.ver = verMax+1;
                            objEntity.Validate_TCKT01s.Add(objUser);
                            objEntity.SaveChanges();
                        }
                    }
                    else
                    {
                        message = "Định dạng file không được hỗ trợ!!!";
                        listmessage.Add(message);
                    }
                }
                else
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            return listmessage;
        }

        [HttpGet, Route("validate")]
        public async Task<IHttpActionResult> GetValidate()
        {
            using (var db = new TCEntities())
            {
                //List<F_CSKH_05> bc = new List<F_CSKH_05>();
                /*List<int?> ver = await (from bc in db.F_CSKH_05
                                        select bc.version).Distinct().OrderByDescending(x=>x.Value).ToListAsync();*/

                //List<int?> ver = await db.F_CSKH_05.Select(o=>o.version).Distinct().OrderByDescending(x=>x.Value).ToListAsync();
                int? verMax = db.Validate_TCKT01s.Max(x => x.ver);
                var tckt01Validate = await db.Validate_TCKT01s.Where(x => x.ver == verMax).ToListAsync();

                //var ver = await db.F_CSKH_05.ToListAsync();
                //List<int?> ver = await db.F_CSKH_05.Select(o => o.version).OrderByDescending().Distinct().ToListAsync();
                //bc = await db.F_CSKH_05.GroupBy(x=>x.version).Select(g=>g.FirstOrDefault()).ToListAsync();

                if (tckt01Validate == null)
                    return NotFound();

                return Ok(tckt01Validate);
            }
        }
    }
}
