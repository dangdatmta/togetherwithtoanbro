﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;
namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/daily")]
    public class DaiLyController : BaseApiController
    {
        // GET: DaiLy
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").ToListAsync();
                var daily = await db.DaiLies.ToListAsync();

                //var user = await db.Users.Include("UserRole").ToListAsync();
                if (daily == null)
                    return NotFound();
                return Ok(daily);
            }
        }
        [HttpGet, Route("{dailyID}")]
        public async Task<IHttpActionResult> GetById(int dailyID)
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").SingleOrDefaultAsync(o => o.Id == userId);
                var daily = await db.DaiLies.SingleOrDefaultAsync(o => o.id == dailyID);
                if (daily == null)
                    return NotFound();
                return Ok(daily);
            }
        }
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] DaiLy daily)
        {
            using (var db = new TCEntities())
            {
                List<string> listdaily = await db.DaiLies.Select(x => x.ten).ToListAsync();
                foreach (var dl in listdaily)
                {
                    if (dl == daily.ten)
                    {
                        return BadRequest("Đại lý này đã tồn tại!!!");
                    }
                }
                db.DaiLies.Add(daily);
                await db.SaveChangesAsync();
            }
            return Ok(daily);
        }
        [HttpPut, Route("{dailyID}")]
        public async Task<IHttpActionResult> Update(int dailyID, [FromBody] DaiLy daily)
        {
            if (daily.id != dailyID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(daily).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.DaiLies.Count(o => o.id == dailyID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(daily);
            }
        }
        [HttpDelete, Route("{dailyID}")]
        public async Task<String> Delete(int dailyID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var daily = await db.DaiLies.SingleOrDefaultAsync(o => o.id == dailyID);
                var daily_permission_bi = await db.PermissionBireport_DaiLy.Where(o => o.daily_id == dailyID).ToListAsync();
                var daily_vbc = await db.NhomViewBC_DaiLy.Where(o => o.daily_id == dailyID).ToListAsync();
                if (daily == null)
                {
                    message = "Không tìm thấy Đại lý";
                }
                else
                {
                    foreach (var per in daily_permission_bi)
                    {
                        db.Entry(per).State = EntityState.Deleted;
                    }
                    foreach (var vbc in daily_vbc)
                    {
                        db.Entry(vbc).State = EntityState.Deleted;
                    }
                    db.Entry(daily).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }

                return message;
            }
        }
    }
}