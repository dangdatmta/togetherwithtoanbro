﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/khuvuc")]
    public class KhuVucController : BaseApiController
    {
        // GET: KhuVuc
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").ToListAsync();
                var khuvuc = await db.KhuVucs.ToListAsync();

                //var user = await db.Users.Include("UserRole").ToListAsync();
                if (khuvuc == null)
                    return NotFound();
                return Ok(khuvuc);
            }
        }
        [HttpGet, Route("{khuvucID}")]
        public async Task<IHttpActionResult> GetById(int khuvucID)
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").SingleOrDefaultAsync(o => o.Id == userId);
                var khuvuc = await db.KhuVucs.SingleOrDefaultAsync(o => o.id == khuvucID);
                if (khuvuc == null)
                    return NotFound();
                return Ok(khuvuc);
            }
        }
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] KhuVuc khuvuc)
        {
            using (var db = new TCEntities())
            {
                List<string> listkhuvuc = await db.KhuVucs.Select(x => x.ten).ToListAsync();
                foreach (var tinh in listkhuvuc)
                {
                    if (tinh == khuvuc.ten)
                    {
                        return BadRequest("Khu vực này đã tồn tại!!!");
                    }
                }
                db.KhuVucs.Add(khuvuc);
                await db.SaveChangesAsync();
            }
            return Ok(khuvuc);
        }
        [HttpPut, Route("{khuvucID}")]
        public async Task<IHttpActionResult> Update(int khuvucID, [FromBody] KhuVuc khuvuc)
        {
            if (khuvuc.id != khuvucID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(khuvuc).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.KhuVucs.Count(o => o.id == khuvucID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(khuvuc);
            }
        }
        [HttpDelete, Route("{khuvucID}")]
        public async Task<String> Delete(int khuvucID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var khuvuc = await db.KhuVucs.SingleOrDefaultAsync(o => o.id == khuvucID);
                var khuvuc_tinhthanh = await db.TinhThanhs.Where(o => o.khuvuc_id == khuvucID).ToListAsync();
                List<int> listidtinhthanh = await db.TinhThanhs.Where(o => o.khuvuc_id == khuvucID).Select(x => x.id).ToListAsync();
                

                var khuvuc_per_bi = await db.PermissionBireport_KhuVuc.Where(o => o.khuvuc_id == khuvucID).ToListAsync();
                var khuvuc_vbc = await db.NhomViewBC_KhuVuc.Where(o => o.khuvuc_id == khuvucID).ToListAsync();
                if (khuvuc == null)
                {
                    message = "Không tìm thấy khu vực";
                }
                else
                {
                    TinhThanhController ltt = new TinhThanhController();
                    string m1;
                    foreach (var item in listidtinhthanh)
                    {
                        m1 = await ltt.Delete(item);
                    }
                    foreach (var per in khuvuc_per_bi)
                    {
                        db.Entry(per).State = EntityState.Deleted;
                    }
                    foreach (var vbc in khuvuc_vbc)
                    {
                        db.Entry(vbc).State = EntityState.Deleted;
                    }               
                    db.Entry(khuvuc).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa khu vực thành thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }

                return message;
            }
        }
    }
}