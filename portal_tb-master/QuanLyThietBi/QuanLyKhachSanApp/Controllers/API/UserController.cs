﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/user")]
    public class UserController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var user = await db.Users.Include(x => x.User_BoPhans).Include(x => x.User_NhomNSDs)
                                         .Include("User_BoPhans.BoPhan").Include("User_NhomNSDs.NhomNSD").ToListAsync();
                //.Include("User_NhomNSD.NhomNSD").Include("NhomNSD.NhomNSD_ChucNang").Include("NhomNSD_ChucNang.ChucNang")
                //var user = await db.Users.ToListAsync();

                //var user = await db.Users.Include("UserRole").ToListAsync();
                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }

        [HttpGet, Route("{userId}")]
        public async Task<IHttpActionResult> GetById(int userId)
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").SingleOrDefaultAsync(o => o.Id == userId);
                var user = await db.Users.SingleOrDefaultAsync(o => o.id == userId);
                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] User user)
        {
            using (var db = new TCEntities())
            {
                List<string> listUserName = await db.Users.Select(x => x.tendangnhap).ToListAsync();
                foreach (var userName in listUserName)
                {
                    if (user.tendangnhap == userName)
                    {
                        return BadRequest("UserName này đã tồn tại!!!");
                    }
                }
                db.Users.Add(user);
                await db.SaveChangesAsync();
            }
            return Ok(user);
        }


        //public async Task<IHttpActionResult> Update(int userID, [FromBody] User user)
        //{
        //    if (user.id != userID) return BadRequest("Id mismatch");

        //    /*if (!ModelState.IsValid)
        //      {
        //          return BadRequest(ModelState);
        //      }*/
        //    using (var db = new TCEntities())
        //    {

        //        var userCurrent = await db.Users.SingleOrDefaultAsync(o => o.id == userID);
        //        //var userRoles = await db.UserRoles.Where(o => o.UserId == userID).ToListAsync();
        //        //var userBSN = await db.UserBussinessGroups.Where(o => o.UserId == userID).ToListAsync();
        //        //var userGrU = await db.UserGroupUsers.Where(o => o.UserId == userID).ToListAsync();
        //        //foreach (var UGR in userGrU)
        //        //{
        //        //    db.Entry(UGR).State = EntityState.Deleted;
        //        //}
        //        //foreach (var UBG in userBSN)
        //        //{
        //        //    db.Entry(UBG).State = EntityState.Deleted;
        //        //}
        //        //foreach (var UR in userRoles)
        //        //{
        //        //    db.Entry(UR).State = EntityState.Deleted;
        //        //}
        //        userCurrent.ten = user.ten;
        //        userCurrent.tendangnhap = user.tendangnhap;
        //        userCurrent.matkhau = user.matkhau;
        //        userCurrent.nguoisua = user.nguoisua;
        //        //db.Entry(userCurrent).State = EntityState.Deleted;
        //        //await db.SaveChangesAsync();

        //        //db.Users.Add(user);

        //        try
        //        {
        //            await db.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException ex)
        //        {
        //            bool exists = db.Users.Count(o => o.id == userID) > 0;
        //            if (!exists)
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw ex;
        //            }
        //        }
        //        return Ok(user);
        //    }
        //}
        [HttpPut, Route("{userId}")]
        public async Task<IHttpActionResult> Update(int userID, [FromBody] User user)
        {
            if (user.id != userID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(user).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.DaiLies.Count(o => o.id == userID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(user);
            }
        }

        [HttpDelete, Route("{userId}")]
        public async Task<String> Delete(int userID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var user = await db.Users.SingleOrDefaultAsync(o => o.id == userID);
                var userBSN = await db.User_BoPhan.Where(o => o.User_id == userID).ToListAsync();
                var userGrU = await db.User_NhomNSD.Where(o => o.user_id == userID).ToListAsync();
                if (user == null)
                {
                    message = "Không tìm thấy User";
                }
                else
                {
                    foreach (var UGR in userGrU)
                    {
                        db.Entry(UGR).State = EntityState.Deleted;
                    }
                    foreach (var UBG in userBSN)
                    {
                        db.Entry(UBG).State = EntityState.Deleted;
                    }
                    db.Entry(user).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa người dùng thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }

                return message;
            }
        }
    }
}