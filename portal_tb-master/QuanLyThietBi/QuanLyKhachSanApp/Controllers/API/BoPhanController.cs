﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/bophan")]
    public class BoPhanController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var bp = await db.BoPhans.Select(x=>new {x.id, x.code, x.text, x.link, x.stt, x.trangthai}).OrderBy(x=>x.stt).ToListAsync();
                //var bp = await db.BoPhans.OrderBy(x=>x.stt).ToListAsync();
                if (bp == null)
                    return NotFound();
                return Ok(bp);
            }
        }

        [HttpGet, Route("submenu")]
        public async Task<IHttpActionResult> GetAllForSubMenu()
        {
            using (var db = new TCEntities())
            {
                var bp = await db.BoPhans.Include("ConfigReports").OrderBy(x=>x.stt).ToListAsync();
                if (bp == null)
                    return NotFound();
                return Ok(bp);
            }
        }

        //Lấy ra các nav của menu có trạng thái active = 1 để show hind menu theo active
        /*   [HttpGet, Route("formenu")]
           public async Task<IHttpActionResult> GetAllForMenu()
           {
               using (var db = new TCEntities())
               {
                   var bp = await db.BoPhans.Select(x => new { x.id, x.text, x.link, x.stt, x.trangthai }).Where(o=>o.trangthai==1).OrderBy(x => x.stt).ToListAsync();
                   //var bp = await db.BoPhans.OrderBy(x=>x.stt).ToListAsync();
                   if (bp == null)
                       return NotFound();
                   return Ok(bp);
               }
           }
   */
        /*        [HttpGet, Route("forsubmenu")]
                public async Task<IHttpActionResult> GetAllForSubMenuFlActive()
                {
                    using (var db = new TCEntities())
                    {
                        var bp = await db.BoPhans.Select(o=>new {o.stt, o.ConfigReports }).Include(m=>m.ConfigReports.Where(g=>g.trangthai == 1)).OrderBy(x => x.stt).ToListAsync();
                        if (bp == null)
                            return NotFound();
                        return Ok(bp);
                    }
                }*/
        //
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] BoPhan bp)
        {
            using (var db = new TCEntities())
            {
                var listbp = await db.BoPhans.Select(x => x.text).ToListAsync();
                foreach (var name in listbp)
                {
                    if (name == bp.text)
                    {
                        return BadRequest("Bộ phận này đã tồn tại!!!");
                    }
                }
                db.BoPhans.Add(bp);
                await db.SaveChangesAsync();
            }
            return Ok(bp);
        }
        [HttpPut, Route("{bpID}")]
        public async Task<IHttpActionResult> Update(int bpID, [FromBody] BoPhan bp)
        {
            if (bp.id != bpID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(bp).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.NhomLoaiDuLieux.Count(o => o.id == bpID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(bp);
            }
        }
        /*[HttpDelete, Route("{bpID}")]
        public async Task<String> Delete(int bpID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var bophan = await db.BoPhans.SingleOrDefaultAsync(o => o.id == bpID);
                var nhomloaidulieu = await db.NhomLoaiDuLieux.Where(o => o.bophan_id == bpID).ToListAsync();
                var configreports = await db.ConfigReports.Where(o => o.bophan_id == bpID).ToListAsync();
                var user_bophans = await db.User_BoPhan.Where(o => o.BoPhan_id == bpID).ToListAsync();
                if (bophan == null)
                {
                    message = "Không tìm thấy Bộ phận!!!";
                }
                else
                {
                    foreach (var nldl in nhomloaidulieu)
                    {
                        var tckt01s = await db.TCKT_01.Where(o => o.nhomloaidulieu_id == nldl.id).ToListAsync();
                        foreach (var tckt01 in tckt01s)
                        {
                            db.Entry(tckt01).State = EntityState.Deleted;
                        }
                        db.Entry(nldl).State = EntityState.Deleted;
                    }
                    foreach (var cfrp in configreports)
                    {
                        db.Entry(cfrp).State = EntityState.Deleted;
                    }
                    foreach (var u_bp in user_bophans)
                    {
                        db.Entry(u_bp).State = EntityState.Deleted;
                    }
                    db.Entry(bophan).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }
                return message;
            }
        }*/
    }
}
