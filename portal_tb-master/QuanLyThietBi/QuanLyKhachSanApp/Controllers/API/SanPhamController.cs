﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;
namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/sanpham")]
    public class SanPhamController : BaseApiController
    {
        // GET: DaiLy
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                //var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").ToListAsync();
                var sanpham = await db.SanPhams.ToListAsync();

                //var user = await db.Users.Include("UserRole").ToListAsync();
                if (sanpham == null)
                    return NotFound();
                return Ok(sanpham);
            }
        }
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] SanPham sanpham)
        {
            using (var db = new TCEntities())
            {
                var listsanpham = await db.SanPhams.Select(x => x.ten).ToListAsync();
                foreach (var sp in listsanpham)
                {
                    if (sp == sanpham.ten)
                    {
                        return BadRequest("Sản phẩm này đã tồn tại!!!");
                    }
                }
                db.SanPhams.Add(sanpham);
                await db.SaveChangesAsync();
            }
            return Ok(sanpham);
        }
        [HttpPut, Route("{sanphamID}")]
        public async Task<IHttpActionResult> Update(int sanphamID, [FromBody] SanPham sanpham)
        {
            if (sanpham.id != sanphamID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(sanpham).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.SanPhams.Count(o => o.id == sanphamID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(sanpham);
            }
        }
        [HttpDelete, Route("{sanphamID}")]
        public async Task<String> Delete(int sanphamID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var sanpham = await db.SanPhams.SingleOrDefaultAsync(o => o.id == sanphamID);
                var sanpham_permission_bi = await db.PermissionBireport_SanPham.Where(o => o.sanpham_id == sanphamID).ToListAsync();
                var sanpham_vbc = await db.NhomViewBC_SanPham.Where(o => o.sanpham_id == sanphamID).ToListAsync();
                if (sanpham == null)
                {
                    message = "Không tìm thấy Sản Phẩm";
                }
                else
                {
                    foreach (var per in sanpham_permission_bi)
                    {
                        db.Entry(per).State = EntityState.Deleted;
                    }
                    foreach (var vbc in sanpham_vbc)
                    {
                        db.Entry(vbc).State = EntityState.Deleted;
                    }
                    db.Entry(sanpham).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }

                return message;
            }
        }
    }
}