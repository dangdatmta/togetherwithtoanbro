using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using CMS.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/monhoc")]
    public class MonHocController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Search([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<MonHoc> results = db.MonHoc;
                if (pagination == null)
                    pagination = new Pagination();
                if (!pagination.includeEntities)
                { 
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.TenMon.Contains(keywords));

                results = results.OrderBy(o => o.MonHocID);

                return Ok((await GetPaginatedResponse(results, pagination)));
            }
        }
         
        [HttpGet, Route("{monHocID:int}")]
        public async Task<IHttpActionResult> Get(int monHocID)
        {
            using (var db = new ApplicationDbContext())
            {
                var monHoc = await db.MonHoc
                    .SingleOrDefaultAsync(o => o.MonHocID == monHocID);

                if (monHoc == null)
                    return NotFound();

                return Ok(monHoc);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] MonHoc monHoc)
        {
            if (monHoc.MonHocID != 0) return BadRequest("Invalid MonHocID");

            using (var db = new ApplicationDbContext())
            {
                db.MonHoc.Add(monHoc);
                await db.SaveChangesAsync();
            }

            return Ok(monHoc);
        }

        [HttpPut, Route("{monHocID:int}")]
        public async Task<IHttpActionResult> Update(int monHocID, [FromBody] MonHoc monHoc)
        {
            if (monHoc.MonHocID != monHocID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            {
                db.Entry(monHoc).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.MonHoc.Count(o => o.MonHocID == monHocID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(monHoc);
            }
        }

        [HttpDelete, Route("{monHocID:int}")]
        public async Task<IHttpActionResult> Delete(int monHocID)
        {
            using (var db = new ApplicationDbContext())
            {
                var monHoc = await db.MonHoc.SingleOrDefaultAsync(o => o.MonHocID == monHocID);

                if (monHoc == null)
                    return NotFound();

                db.Entry(monHoc).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}
