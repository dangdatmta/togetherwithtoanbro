﻿using HVIT.Security;
using HVITCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Data.Entity;
using QuanLyKhachSanApp.Models;
using System.Threading.Tasks;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApiController
    {
        public class LoginForm
        {
            public string tendangnhap { get; set; }
            public string matkhau { get; set; }
            public string RePassword { get; set; }
            public string ChangePasswordKey { get; set; }
            public string OldPassword { get; set; }
        }

        public class UserModel
        {
            public User User { get; set; }
            public List<int> Groups { get; set; }

            public UserModel()
            {
                Groups = new List<int>();
            }
        }

        [HttpPost]
        [Route("login")]
        public async Task<IHttpActionResult> Login(LoginForm login)
        {
            using (TCEntities db = new TCEntities())
            {
                //User user1 = db.Users.Where(x=>x.tendangnhap == login.userName).Include(x => x.User_ChucNang.Select(o=>o.Role.RoleRoleDetails.Select(u=>u.RoleDetail))).FirstOrDefault();
                User user1 = await db.Users.Where(x => x.tendangnhap == login.tendangnhap).Include(x => x.User_BoPhans).FirstOrDefaultAsync();
                User user = await db.Users.SingleOrDefaultAsync(x => x.tendangnhap == login.tendangnhap);
                if (user != null)
                {
                    string passwordInput = AuthenticationHelper.getPassWord(login.matkhau);
                    string passwordUser = user.matkhau;

                    if (passwordInput.Equals(passwordUser))
                    {

                        TokenProvider tokenProvider = new TokenProvider();
                        TokenIdentity token = tokenProvider.GenerateToken(user.id, user.ten,
                            Request.Headers.UserAgent.ToString(),
                            "", DateTime.Now.Ticks);
                        token.SetAuthenticationType("Custom");
                        token.SetIsAuthenticated(true);
                        db.AccessTokenss.Add(new AccessTokens()
                        {
                            Token = token.Token,
                            EffectiveTime = new DateTime(token.EffectiveTime),
                            ExpiresIn = token.ExpiresTime,
                            IP = token.IP,
                            UserAgent = token.UserAgent,
                            UserName = token.Name,
                            UserId = user.id,
                        });
                        await db.SaveChangesAsync();

                        var users = await db.Users
                            .Select(x => new
                            {
                                x.id,
                                x.tendangnhap,
                                x.ten,
                                x.sdt
                            })
                            .FirstOrDefaultAsync(x => x.id == user.id);
                        return Ok(
                            new
                            {
                                AccessToken = token,
                                Profile = new
                                {
                                    id = user1.id,
                                    tendangnhap = user1.tendangnhap,
                                    ten = user1.ten,
                                    //Roles = user1.User_ChucNang.Select(x => x.ChucNang.ten),
                                    //BoPhans = user1.User_BoPhan.Select(x => x.BoPhan.text),
                                    RoleDetails = user1,
                                    trangthai = user1.trangthai,
                                    //countUserRole = user1.User_ChucNang.Count(),
                                    countUserBoPhan = user1.User_BoPhans.Count(),
                                    //countRoleRoleDetails = user1.UserRoles.Select(x=>x.Role.RoleRoleDetails.Count())
                                    //NhanVien = nhanVien,
                                }
                            }
                        );
                    }
                }
                return Ok("Login failed!");
            }
        }

   /*     [AuthorizeUser, HttpGet]
        [Route("logout")]
        public IHttpActionResult Logout(Guid UserID)
        {
            using (var db = new TCEntities())
            {
                var user = db.Users.Where(x => x.Id == UserID).FirstOrDefault();
                if (user == null)
                    return BadRequest("invalid UserID");
                db.SaveChanges();
                return Ok();
            }
        }*/

        [AuthorizeUser, HttpGet]
        [Route("validate-token")]
        public IHttpActionResult ValidateToken()
        {
            TokenIdentity tokenIdentity = ClaimsPrincipal.Current.Identity as TokenIdentity;
            return Ok();
        }

    }
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            filterContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }
}
