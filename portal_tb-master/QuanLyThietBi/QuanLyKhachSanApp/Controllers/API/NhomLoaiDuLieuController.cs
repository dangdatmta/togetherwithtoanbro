﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Net.Http.Handlers;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using QuanLyKhachSanApp.Models;
using System.Web;
using System.IO;
using System.Net.Http;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/nhomloaidulieu")]
    public class NhomLoaiDuLieuController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var ldl = await db.NhomLoaiDuLieux.Include(x=>x.BoPhan).ToListAsync();
                if (ldl == null)
                    return NotFound();
                return Ok(ldl);
            }
        }

        [HttpGet, Route("{bophan_id}")]
        public async Task<IHttpActionResult> GetNLDLByBoPhanId(int bophan_id)
        {
            using (var db = new TCEntities())
            {
                var ldl = await db.NhomLoaiDuLieux.Where(x=>x.bophan_id == bophan_id).ToListAsync();
                if (ldl == null)
                    return NotFound();
                return Ok(ldl);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert()
        {
            var httpRequest = HttpContext.Current.Request;
            var fileName = "";
               

            using (var db = new TCEntities())
            {
                NhomLoaiDuLieu nldl = new NhomLoaiDuLieu();
                if (httpRequest.Files.Count > 0)
                {
                    

                    fileName = Path.GetFileName(httpRequest.Files[0].FileName);

                    string nameModify = String.Join("", HttpContext.Current.Request.Params["name"].Split('\"', '\"'));
                    string codeModify = String.Join("", HttpContext.Current.Request.Params["code"].Split('\"', '\"'));
                    string loaikyModify = String.Join("", HttpContext.Current.Request.Params["loaiky"].Split('\"', '\"'));
                    string bophan_idModify =  String.Join("", HttpContext.Current.Request.Params["bophan_id"].Split('\"', '\"'));
                    nldl.name = nameModify;
                    nldl.code = codeModify;
                    nldl.loaiky = int.Parse(loaikyModify);
                    nldl.bophan_id = int.Parse(bophan_idModify);
                    nldl.filetemplate = fileName;
                    var path = Path.Combine(HttpContext.Current.Server.MapPath("~/FileUpload"), fileName);
                    httpRequest.Files[0].SaveAs(path);
                    var listnldl = await db.NhomLoaiDuLieux.Select(x => x.name).ToListAsync();
                    foreach (var name in listnldl)
                    {
                        if (name == nldl.name)
                        {
                            return BadRequest("Nhóm loại dữ liệu này đã tồn tại!!!");
                        }
                    }
                    db.NhomLoaiDuLieux.Add(nldl);
                    await db.SaveChangesAsync();

                    
                    var nldlID_max = await db.NhomLoaiDuLieux.MaxAsync(x => x.id);
                    var loaikyThang_id = await db.KyBaoCaos.Where(o => o.loaiky == 1).Select(x => x.id).ToListAsync();
                    var loaikyQuy_id = await db.KyBaoCaos.Where(o => o.loaiky == 2).Select(x => x.id).ToListAsync();
                    var loaikyNam_id = await db.KyBaoCaos.Where(o => o.loaiky == 3).Select(x => x.id).ToListAsync();
                    NLDL_KyBC nldl_kybc = new NLDL_KyBC();
                    if (nldl.loaiky == 1)
                    {
                        foreach (var id in loaikyThang_id)
                        {

                            nldl_kybc.kybc_id = id;
                            nldl_kybc.NLDL_id = nldlID_max;
                            db.NLDL_KyBCs.Add(nldl_kybc);
                            await db.SaveChangesAsync();
                        }
                    }
                    else if (nldl.loaiky == 2)
                    {
                        foreach (var id in loaikyQuy_id)
                        {
                            nldl_kybc.kybc_id = id;
                            nldl_kybc.NLDL_id = nldlID_max;
                            db.NLDL_KyBCs.Add(nldl_kybc);
                            await db.SaveChangesAsync();
                        }
                    }
                    else if (nldl.loaiky == 3)
                    {
                        foreach (var id in loaikyNam_id)
                        {
                            nldl_kybc.kybc_id = id;
                            nldl_kybc.NLDL_id = nldlID_max;
                            db.NLDL_KyBCs.Add(nldl_kybc);
                            await db.SaveChangesAsync();
                        }
                    }
                }
                return Ok(nldl);
            }
        }
        [HttpPut, Route("{nldlID}")]
        public async Task<IHttpActionResult> Update(int nldlID, [FromBody] NhomLoaiDuLieu nldl)
        {
            if (nldl.id != nldlID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(nldl).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.NhomLoaiDuLieux.Count(o => o.id == nldlID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(nldl);
            }
        }
        [HttpDelete, Route("{nldlID}")]
        public async Task<String> Delete(int nldlID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var nldl = await db.NhomLoaiDuLieux.SingleOrDefaultAsync(o => o.id == nldlID);
                var nldl_kybcs = await db.NLDL_KyBCs.Where(o => o.NLDL_id == nldlID).ToListAsync();
                if (nldl == null)
                {
                    message = "Không tìm thấy NLDL!!!";
                }
                else
                {
                    foreach (var nldl_kybc in nldl_kybcs)
                    {
                        var tckt01s = await db.TCKT_01.Where(o => o.nldl_kybc_id == nldl_kybc.id).ToListAsync();
                        foreach (var tckt01 in tckt01s)
                        {
                            db.Entry(tckt01).State = EntityState.Deleted;
                        }
                        db.Entry(nldl_kybc).State = EntityState.Deleted;
                    }
                    db.Entry(nldl).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }
                return message;
            }
        }
    }
}
