﻿/*using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;
namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/kybaocao")]
    public class KyBaoCaoController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var kybaocao = await db.KyBaoCaos.OrderByDescending(x=>x.nam).ToListAsync();
                if (kybaocao == null)
                    return NotFound();
                return Ok(kybaocao);
            }
        }
        [HttpGet, Route("year")]
        public async Task<IHttpActionResult> GetYear()
        {
            using (var db = new TCEntities())
            {
                var year = await db.KyBaoCaos.Select(x=>x.nam).Distinct().ToListAsync();
                if (year == null)
                    return NotFound();
                return Ok(year);
            }
        }
        [HttpGet, Route("{year},{loaiDL_id}")]
        public List<KyBaoCao> GetLoaiKyFollowYearAnLloaiDl(int year, int loaiDL_id)
        {
            using (var db = new TCEntities())
            {
                var kyBCid = db.NLDL_KyBCs.Where(o => o.NLDL_id == loaiDL_id).Select(x => x.kybc_id).FirstOrDefault();
                var loaiKyBC = db.KyBaoCaos.Where(o => o.id == kyBCid).Select(x => x.loaiky).FirstOrDefault();
                var listNameLoaiKy = new List<KyBaoCao>();
                if (loaiKyBC == 1)
                {
                    listNameLoaiKy = db.KyBaoCaos.Where(o => o.loaiky == 1 && o.nam == year).ToList();
                }else if (loaiKyBC == 2)
                {
                     listNameLoaiKy = db.KyBaoCaos.Where(o => o.loaiky == 2 && o.nam == year).ToList();
                }
                else if (loaiKyBC == 3)
                {
                    listNameLoaiKy = db.KyBaoCaos.Where(o => o.loaiky == 3 && o.nam == year).ToList();
                }
                return listNameLoaiKy;
            }
        }
        [HttpPost, Route("{year}")]
        public async Task<IHttpActionResult> Insert(int year)
        {
            KyBaoCao kybaocao = new KyBaoCao();
            using (var db = new TCEntities())
            {
                for(int i = 1; i < 383; i++)
                {
                    kybaocao.ngay = null;
                    kybaocao.nam = year;
                    kybaocao.trangthai = 1;
                    kybaocao.thoigiantao = DateTime.Now;
                    switch (i)
                    {
                        case 1:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "01/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "01/" + "31");
                            kybaocao.quy = 1;
                            kybaocao.thang = 1;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.ten = year+"-"+"T1";
                            kybaocao.loaiky = 1;
                            break;
                        case 2:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "02/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "02/" + "28");
                            kybaocao.quy = 1;
                            kybaocao.thang = 2;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T2";
                            break;
                        case 3:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "03/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "03/" + "31");
                            kybaocao.quy = 1;
                            kybaocao.thang = 3;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T3";
                            break;
                        case 4:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "04/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "04/" + "30");
                            kybaocao.quy = 2;
                            kybaocao.thang = 4;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T4";
                            break;
                        case 5:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "05/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "05/" + "31");
                            kybaocao.quy = 2;
                            kybaocao.thang = 5;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T5";
                            break;
                        case 6:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "06/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "06/" + "30");
                            kybaocao.quy = 2;
                            kybaocao.thang = 6;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T6";
                            break;
                        case 7:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "07/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "07/" + "31");
                            kybaocao.quy = 3;
                            kybaocao.thang = 7;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T7";
                            break;
                        case 8:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "08/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "08/" + "31");
                            kybaocao.quy = 3;
                            kybaocao.thang = 8;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T8";
                            break;
                        case 9:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "09/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "09/" + "30");
                            kybaocao.quy = 3;
                            kybaocao.thang = 9;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T9";
                            break;
                        case 10:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "10/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "10/" + "31");
                            kybaocao.quy = 4;
                            kybaocao.thang = 10;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T10";
                            break;
                        case 11:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "11/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "11/" + "30");
                            kybaocao.quy = 4;
                            kybaocao.thang = 11;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T11";
                            break;
                        case 12:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "12/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "12/" + "31");
                            kybaocao.quy = 4;
                            kybaocao.thang = 12;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T12";
                            break;
                        case 13:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "01/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "03/" + "31");
                            kybaocao.quy = 1;
                            kybaocao.ghichu = "Period Quarter - Year " + year;
                            kybaocao.loaiky = 2;
                            kybaocao.thang = null;
                            kybaocao.ten = year + "-" + "Q1";
                            break;
                        case 14:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "04/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "06/" + "30");
                            kybaocao.quy = 2;
                            kybaocao.ghichu = "Period Quarter - Year " + year;
                            kybaocao.loaiky = 2;
                            kybaocao.thang = null;
                            kybaocao.ten = year + "-" + "Q2";
                            break;
                        case 15:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "07/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "09/" + "30");
                            kybaocao.quy = 3;
                            kybaocao.ghichu = "Period Quarter - Year " + year;
                            kybaocao.loaiky = 2;
                            kybaocao.thang = null;
                            kybaocao.ten = year + "-" + "Q3";
                            break;
                        case 16:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "10/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "12/" + "31");
                            kybaocao.quy = 4;
                            kybaocao.ghichu = "Period Quarter - Year " + year;
                            kybaocao.loaiky = 2;
                            kybaocao.thang = null;
                            kybaocao.ten = year + "-" + "Q4";
                            break;
                        case 17:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "01/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "12/" + "31");
                            kybaocao.ghichu = "Period Year - Year " + year;
                            kybaocao.loaiky = 3;
                            kybaocao.quy = null;
                            kybaocao.ten = year.ToString();
                            break;
                        default:
                            if ()
                            {
                                kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "01/" + "01");
                                kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "12/" + "31");
                            }
                            
                            kybaocao.ghichu = "Period Day - Year " + year;
                            kybaocao.loaiky = 4;
                            kybaocao.quy = null;
                            kybaocao.ten = kybaocao.ngaybatdau.ToString();
                            break;
                    }
                    db.KyBaoCaos.Add(kybaocao);
                    await db.SaveChangesAsync();
                }       
            }
            return Ok(kybaocao);
        }
        [HttpPut, Route("{kybaocaoID}")]
        public async Task<IHttpActionResult> Update(int kybaocaoID, [FromBody] KyBaoCao kybaocao)
        {
            if (kybaocao.id != kybaocaoID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(kybaocao).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.KyBaoCaos.Count(o => o.id == kybaocaoID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(kybaocao);
            }
        }
        [HttpDelete, Route("{kybaocaoID}")]
        public async Task<String> Delete(int kybaocaoID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var kybaocao = await db.KyBaoCaos.SingleOrDefaultAsync(o => o.id == kybaocaoID);
                var nhomloaidulieu_kybcs = await db.NLDL_KyBCs.Where(o => o.kybc_id == kybaocaoID).ToListAsync();
                //var tckt01 = await db.TCKT_01.Where(o => o.nhomloaidulieu_id == nhomloaidulieu.All(x=>x.id)).ToListAsync();
                if (kybaocao == null)
                {
                    message = "Không tìm thấy kỳ báo cáo!!!";
                }
                else
                {
                    foreach (var nldl_kybc in nhomloaidulieu_kybcs)
                    {
                        var tckt01s = await db.TCKT_01.Where(o => o.nldl_kybc_id == nldl_kybc.id).ToListAsync();
                        foreach (var tckt01 in tckt01s)
                        {
                            db.Entry(tckt01).State = EntityState.Deleted;
                        }
                        db.Entry(nldl_kybc).State = EntityState.Deleted;
                    }
                    
                    db.Entry(kybaocao).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }
                return message;
            }
        }
    }
}*/

using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;
namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/kybaocao")]
    public class KyBaoCaoController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var kybaocao = await db.KyBaoCaos.OrderByDescending(x => x.nam).ToListAsync();
                if (kybaocao == null)
                    return NotFound();
                return Ok(kybaocao);
            }
        }
        [HttpGet, Route("{nldl_id}")]
        public async Task<IHttpActionResult> GetKyFollowNLDL_id(int nldl_id)
        {
            using (var db = new TCEntities())
            {
                List<KyBaoCao> kybaocaos = new List<KyBaoCao>();
                var kybc_ids = await db.NLDL_KyBCs.Where(x => x.NLDL_id == nldl_id).Select(o=>o.kybc_id).ToListAsync();
                
                foreach (var kybc_id in kybc_ids)
                {
                    var kybaocao = db.KyBaoCaos.Where(x => x.id == kybc_id).GroupBy(x => x.ten).Select(o=>o.FirstOrDefault()).FirstOrDefault();
                    kybaocaos.Add(kybaocao);
                }
                if (kybaocaos == null)
                    return NotFound();
                return Ok(kybaocaos);
            }
        }
        [HttpGet, Route("year")]
        public async Task<IHttpActionResult> GetYear()
        {
            using (var db = new TCEntities())
            {
                var year = await db.KyBaoCaos.Select(x => x.nam).Distinct().ToListAsync();
                if (year == null)
                    return NotFound();
                return Ok(year);
            }
        }
        [HttpGet, Route("{year},{loaiDL_id}")]
        public async Task<List<KyBaoCao>> GetLoaiKyFollowYearAnLloaiDl(int year, int loaiDL_id)
        {
            using (var db = new TCEntities())
            {
                var kyBCid = await db.NLDL_KyBCs.Where(o => o.NLDL_id == loaiDL_id).Select(x => x.kybc_id).FirstOrDefaultAsync();
                var loaiKyBC = await db.KyBaoCaos.Where(o => o.id == kyBCid).Select(x => x.loaiky).FirstOrDefaultAsync();
                var listNameLoaiKy = new List<KyBaoCao>();
                if (loaiKyBC == 1)
                {
                    listNameLoaiKy = db.KyBaoCaos.Where(o => o.loaiky == 1 && o.nam == year).ToList();
                }
                else if (loaiKyBC == 2)
                {
                    listNameLoaiKy = db.KyBaoCaos.Where(o => o.loaiky == 2 && o.nam == year).ToList();
                }
                else if (loaiKyBC == 3)
                {
                    listNameLoaiKy = db.KyBaoCaos.Where(o => o.loaiky == 3 && o.nam == year).ToList();
                }
                return listNameLoaiKy;
            }
        }
        [HttpGet, Route("getTrangThai/{loaiDL_id},{KyBC_id}")]
        public async Task<IHttpActionResult> GetTrangThaiLoaiKy(int loaiDL_id, int KyBC_id)
        {
            using (var db = new TCEntities())
            {
                var trangthai = await db.NLDL_KyBCs.Where(x => x.NLDL_id == loaiDL_id && x.kybc_id == KyBC_id).Select(o=>o.trangthai).FirstOrDefaultAsync();
                return Ok(trangthai);
            }
        }
        [HttpPost, Route("{year}")]
        public async Task<IHttpActionResult> Insert(int year)
        {
            KyBaoCao kybaocao = new KyBaoCao();
            using (var db = new TCEntities())
            {
                for (int i = 1; i < 18; i++)
                {
                    kybaocao.nam = year;
                    kybaocao.trangthai = 1;
                    kybaocao.thoigiantao = DateTime.Now;
                    switch (i)
                    {
                        case 1:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "01/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "01/" + "31");
                            kybaocao.quy = 1;
                            kybaocao.thang = 1;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.ten = year + "-" + "T1";
                            kybaocao.loaiky = 1;
                            break;
                        case 2:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "02/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "02/" + "28");
                            kybaocao.quy = 1;
                            kybaocao.thang = 2;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T2";
                            break;
                        case 3:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "03/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "03/" + "31");
                            kybaocao.quy = 1;
                            kybaocao.thang = 3;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T3";
                            break;
                        case 4:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "04/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "04/" + "30");
                            kybaocao.quy = 2;
                            kybaocao.thang = 4;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T4";
                            break;
                        case 5:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "05/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "05/" + "31");
                            kybaocao.quy = 2;
                            kybaocao.thang = 5;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T5";
                            break;
                        case 6:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "06/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "06/" + "30");
                            kybaocao.quy = 2;
                            kybaocao.thang = 6;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T6";
                            break;
                        case 7:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "07/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "07/" + "31");
                            kybaocao.quy = 3;
                            kybaocao.thang = 7;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T7";
                            break;
                        case 8:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "08/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "08/" + "31");
                            kybaocao.quy = 3;
                            kybaocao.thang = 8;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T8";
                            break;
                        case 9:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "09/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "09/" + "30");
                            kybaocao.quy = 3;
                            kybaocao.thang = 9;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T9";
                            break;
                        case 10:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "10/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "10/" + "31");
                            kybaocao.quy = 4;
                            kybaocao.thang = 10;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T10";
                            break;
                        case 11:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "11/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "11/" + "30");
                            kybaocao.quy = 4;
                            kybaocao.thang = 11;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T11";
                            break;
                        case 12:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "12/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "12/" + "31");
                            kybaocao.quy = 4;
                            kybaocao.thang = 12;
                            kybaocao.ghichu = "Period Month - Year " + year;
                            kybaocao.loaiky = 1;
                            kybaocao.ten = year + "-" + "T12";
                            break;
                        case 13:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "01/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "03/" + "31");
                            kybaocao.quy = 1;
                            kybaocao.ghichu = "Period Quarter - Year " + year;
                            kybaocao.loaiky = 2;
                            kybaocao.thang = null;
                            kybaocao.ten = year + "-" + "Q1";
                            break;
                        case 14:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "04/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "06/" + "30");
                            kybaocao.quy = 2;
                            kybaocao.ghichu = "Period Quarter - Year " + year;
                            kybaocao.loaiky = 2;
                            kybaocao.thang = null;
                            kybaocao.ten = year + "-" + "Q2";
                            break;
                        case 15:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "07/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "09/" + "30");
                            kybaocao.quy = 3;
                            kybaocao.ghichu = "Period Quarter - Year " + year;
                            kybaocao.loaiky = 2;
                            kybaocao.thang = null;
                            kybaocao.ten = year + "-" + "Q3";
                            break;
                        case 16:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "10/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "12/" + "31");
                            kybaocao.quy = 4;
                            kybaocao.ghichu = "Period Quarter - Year " + year;
                            kybaocao.loaiky = 2;
                            kybaocao.thang = null;
                            kybaocao.ten = year + "-" + "Q4";
                            break;
                        default:
                            kybaocao.ngaybatdau = DateTime.Parse(year + "/" + "01/" + "01");
                            kybaocao.ngayketthuc = DateTime.Parse(year + "/" + "12/" + "31");
                            kybaocao.ghichu = "Period Year - Year " + year;
                            kybaocao.loaiky = 3;
                            kybaocao.quy = null;
                            kybaocao.ten = year.ToString();
                            break;
                    }
                    db.KyBaoCaos.Add(kybaocao);
                    await db.SaveChangesAsync();
                }
            }
            return Ok(kybaocao);
        }
        [HttpPut, Route("{kybaocaoID}")]
        public async Task<IHttpActionResult> Update(int kybaocaoID, [FromBody] KyBaoCao kybaocao)
        {
            if (kybaocao.id != kybaocaoID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(kybaocao).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.KyBaoCaos.Count(o => o.id == kybaocaoID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(kybaocao);
            }
        }
        [HttpDelete, Route("{kybaocaoID}")]
        public async Task<String> Delete(int kybaocaoID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var kybaocao = await db.KyBaoCaos.SingleOrDefaultAsync(o => o.id == kybaocaoID);
                var nhomloaidulieu_kybcs = await db.NLDL_KyBCs.Where(o => o.kybc_id == kybaocaoID).ToListAsync();
                //var tckt01 = await db.TCKT_01.Where(o => o.nhomloaidulieu_id == nhomloaidulieu.All(x=>x.id)).ToListAsync();
                if (kybaocao == null)
                {
                    message = "Không tìm thấy kỳ báo cáo!!!";
                }
                else
                {
                    foreach (var nldl_kybc in nhomloaidulieu_kybcs)
                    {
                        var tckt01s = await db.TCKT_01.Where(o => o.nldl_kybc_id == nldl_kybc.id).ToListAsync();
                        foreach (var tckt01 in tckt01s)
                        {
                            db.Entry(tckt01).State = EntityState.Deleted;
                        }
                        db.Entry(nldl_kybc).State = EntityState.Deleted;
                    }

                    db.Entry(kybaocao).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }
                return message;
            }
        }

        [HttpPut, Route("{nldl_id},{kybc_id}")]
        public async Task<IHttpActionResult> DongMoKy(int nldl_id, int kybc_id)
        {
            using (var db = new TCEntities())
            {
                var id_nldl_kybc = await db.NLDL_KyBCs.Where(x => x.NLDL_id == nldl_id && x.kybc_id == kybc_id).Select(o => o.id).FirstOrDefaultAsync();
                if (id_nldl_kybc == null) return BadRequest("Id mismatch");
                var Status_currentNLDL_KyBC = await db.NLDL_KyBCs.Where(x => x.id == id_nldl_kybc).Select(o=>o.trangthai).FirstOrDefaultAsync();
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                NLDL_KyBC nldl_kybc = new NLDL_KyBC();
                nldl_kybc.id = id_nldl_kybc;
                nldl_kybc.NLDL_id = nldl_id;
                nldl_kybc.kybc_id = kybc_id;
                if (Status_currentNLDL_KyBC == 1)
                {
                    nldl_kybc.trangthai = 0;
                }
                else if (Status_currentNLDL_KyBC == 0)
                {
                    nldl_kybc.trangthai = 1;
                }
                db.Entry(nldl_kybc).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.NLDL_KyBCs.Count(o => o.id == id_nldl_kybc) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(nldl_kybc);
            }
        }
    }
}
