 
export interface PhieuMuonTra {
    PhieuMuonTraID: number;
    SoPhieu: string;
    NgayMuon?: Date;
    NgayHenTra?: Date;
    NgayTraThucTe?: Date;
    TietMuonThietBi: string;
    GhiChu: string;
    TongSoTietMuon: number;
    ChiTietPhieu: string;
    NamHoc?: number;
    GiaoVienID: number;
    TrangThaiPhieu?: number;
}