import { DanhMucThietBi } from "./DanhMucThietBi";
import { KhoPhong } from "./KhoPhong";
 
export interface ThietBi {
    ThietBiID: number;
    DanhMucThietBiID?: number;
    KhoPhongID?: number;
    SoLuong?: number;
    SoHieuThietBi: string;
    NguonCapID?: Date;
    KhoiApDung?: string;
    MonApDung?: string;
    HanSuDung?: Date;
    NoiSanXuat?: Date;
    SoHong?: number;
    SoMat?: number;
    TenThietBi: string;
    TenLoaiThietBi: string;
    MaThietBi: string;
    DonViID: string;
    DanhMucThietBi?: DanhMucThietBi[];
    KhoPhong?: KhoPhong[]; 
}
