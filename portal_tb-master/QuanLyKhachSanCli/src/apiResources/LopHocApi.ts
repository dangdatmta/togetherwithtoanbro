import { HTTP } from '@/HTTPServices'
import { BaseApi } from './BaseApi'
import { PaginatedResponse,Pagination } from './PaginatedResponse'
import { LopHoc } from '@/models/LopHoc'
export interface LopHocApiSearchParams extends Pagination {
    keywords?:number; 
} 
class LopHocApi extends BaseApi {
    search(searchParams: LopHocApiSearchParams): Promise<PaginatedResponse<LopHoc>> {

        return new Promise<PaginatedResponse<LopHoc>>((resolve: any, reject: any) => {
            HTTP.get('lophoc', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    } 
    detail(id: number): Promise<LopHoc> {
        return new Promise<LopHoc>((resolve: any, reject: any) => {
            HTTP.get('lophoc/' + id).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    update(id: number, lopHoc: LopHoc): Promise<LopHoc> {
        return new Promise<LopHoc>((resolve: any, reject: any) => {
            HTTP.put('lophoc/' + id, 
                lopHoc
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    insert(lopHoc: LopHoc): Promise<LopHoc> {
        return new Promise<LopHoc>((resolve: any, reject: any) => {
            HTTP.post('lophoc', 
                lopHoc
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    delete(id: number): Promise<LopHoc> {
        return new Promise<LopHoc>((resolve: any, reject: any) => {
            HTTP.delete('lophoc/' + id)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
export default new LopHocApi();
