import { HTTP } from '@/HTTPServices'
import { BaseApi } from './BaseApi'
import { PaginatedResponse,Pagination } from './PaginatedResponse'
import { PhieuGhiTang } from '@/models/PhieuGhiTang'
export interface PhieuGhiTangApiSearchParams extends Pagination {
    keywords?:number; 
} 
class PhieuGhiTangApi extends BaseApi {
    search(searchParams: PhieuGhiTangApiSearchParams): Promise<PaginatedResponse<PhieuGhiTang>> {

        return new Promise<PaginatedResponse<PhieuGhiTang>>((resolve: any, reject: any) => {
            HTTP.get('phieughitang', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    } 
    detail(id: number): Promise<PhieuGhiTang> {
        return new Promise<PhieuGhiTang>((resolve: any, reject: any) => {
            HTTP.get('phieughitang/' + id).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    update(id: number, lopHoc: PhieuGhiTang): Promise<PhieuGhiTang> {
        return new Promise<PhieuGhiTang>((resolve: any, reject: any) => {
            HTTP.put('phieughitang/' + id, 
                lopHoc
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    insert(lopHoc: PhieuGhiTang): Promise<PhieuGhiTang> {
        return new Promise<PhieuGhiTang>((resolve: any, reject: any) => {
            HTTP.post('phieughitang', 
                lopHoc
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    delete(id: number): Promise<PhieuGhiTang> {
        return new Promise<PhieuGhiTang>((resolve: any, reject: any) => {
            HTTP.delete('phieughitang/' + id)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
export default new PhieuGhiTangApi();
