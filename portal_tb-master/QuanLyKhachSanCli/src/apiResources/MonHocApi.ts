import { HTTP } from '@/HTTPServices'
import { BaseApi } from './BaseApi'
import { PaginatedResponse,Pagination } from './PaginatedResponse'
import { MonHoc } from '@/models/MonHoc'
export interface MonHocApiSearchParams extends Pagination {
    keywords?:number; 
} 
class MonHocApi extends BaseApi {
    search(searchParams: MonHocApiSearchParams): Promise<PaginatedResponse<MonHoc>> {

        return new Promise<PaginatedResponse<MonHoc>>((resolve: any, reject: any) => {
            HTTP.get('monhoc', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    } 
    detail(id: number): Promise<MonHoc> {
        return new Promise<MonHoc>((resolve: any, reject: any) => {
            HTTP.get('monhoc/' + id).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    update(id: number, monHoc: MonHoc): Promise<MonHoc> {
        return new Promise<MonHoc>((resolve: any, reject: any) => {
            HTTP.put('monhoc/' + id, 
                monHoc
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    insert(monHoc: MonHoc): Promise<MonHoc> {
        return new Promise<MonHoc>((resolve: any, reject: any) => {
            HTTP.post('monhoc', 
                monHoc
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    delete(id: number): Promise<MonHoc> {
        return new Promise<MonHoc>((resolve: any, reject: any) => {
            HTTP.delete('monhoc/' + id)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
export default new MonHocApi();
