import { ConfigReport } from "./ConfigReport"
import { NhomLoaiDuLieu } from "./NhomLoaiDuLieu"
import { User_BoPhan } from "./User_BoPhan"

export class BoPhan {
    id?: number
    text?: string
    link?: string
    ghichu?: string
    trangthai?: number
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date
    code?: string
    ConfigReports?: ConfigReport[]
    User_BoPhans?: User_BoPhan[]
    NhomLoaiDuLieus?: NhomLoaiDuLieu[]
}