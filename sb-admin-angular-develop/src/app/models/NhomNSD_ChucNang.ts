import { ChucNang } from "./ChucNang"
import { NhomNSD } from "./NhomNSD"

export class NhomNSD_ChucNang {
    int?: number
    nhomnsd_id?: number
    chucnang_id?: number
    NhomNSDs?: NhomNSD
    ChucNangs?: ChucNang
}