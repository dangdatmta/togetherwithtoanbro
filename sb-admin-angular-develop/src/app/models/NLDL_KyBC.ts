import { KyBaoCao } from "./KyBaoCao"
import { NhomLoaiDuLieu } from "./NhomLoaiDuLieu"
import { TCKT01 } from "./TCKT.01"

export class NLDL_KyBC{
    id?: number
    nldl_id?: number
    kybc_id?: number
    TCKT01?: TCKT01[]
    kybaocao?: KyBaoCao
    nhomloaidulieu?: NhomLoaiDuLieu
}