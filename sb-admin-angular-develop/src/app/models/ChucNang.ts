import { NhomNSD_ChucNang } from "./NhomNSD_ChucNang"
export class ChucNang{
    id?: number
    ten?: string
    nhomnsd_chucnangs?: NhomNSD_ChucNang[]
}