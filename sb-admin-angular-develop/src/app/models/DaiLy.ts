import { TinhThanh } from "./TinhThanh"

export class DaiLy {
    id?: number
    ten?: string
    tinhthanh_id?: number
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date
    tinhthanh?: TinhThanh
}