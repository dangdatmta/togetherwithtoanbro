export class Role {
    Id?:number
    Code?:string
    Name?:string
    IsActive?:boolean
    CreatedBy?:string
    CreatedAt?:Date
    ModifiedBy?:string
    ModifiedAt?:Date

}