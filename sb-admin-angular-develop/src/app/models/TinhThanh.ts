import { DaiLy } from "./DaiLy"
import { Region } from "./Region"

export class TinhThanh {
    id?: number
    code?: string
    ten?: string
    khuvuc_id?: number
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date
    KhuVuc?: Region
    DaiLys?: DaiLy[]
}