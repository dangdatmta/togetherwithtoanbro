import { Role } from "./Role";
import { UserRole } from './UserRole';
import { User_BoPhan } from "./User_BoPhan";
import { User_NhomNSD } from "./User_NhomNSD";
export class User {
    id?: number
    ten?: string
    sdt?: string
    email?: string
    tendangnhap?: string
    trangthai?: number
    dangnhaplancuoi?: Date
    tenbophan?: string
    nhomviewbc_id?: number
    permission_bireport_id?: number
    nguoitao?: string
    nguoisua?: string
    thoigiantao?: Date
    thoigiansua?: Date
    matkhau?: string
    // UserRoles?: UserRole[]
    User_BoPhans?: User_BoPhan[]
    User_NhomNSDs?: User_NhomNSD[]
    //Roles?:Role[];
    // UserBussinessGroup: UserBussinessGroup[] = []
}