import { User } from "@testing/mocks"
import { BoPhan } from "./BoPhan";

export class User_BoPhan {

    constructor(bophanId: number) {
        this.BoPhan_id = bophanId;
    }

    id?: number
    BoPhan_id?: number
    User_id?: number
    BoPhan?: BoPhan
    User?: User

}