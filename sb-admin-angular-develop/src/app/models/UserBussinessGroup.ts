import { User } from "@modules/auth/models"
import { BussinessGroup } from "./BussinessGroup"

export class UserBussinessGroup {
    Id?: string
    UserId?: string
    BussinessGroupId?: string
    User?: User
    BussinessGroup?: BussinessGroup
}