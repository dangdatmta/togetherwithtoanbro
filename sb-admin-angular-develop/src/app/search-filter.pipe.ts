import { Pipe, PipeTransform } from '@angular/core';
import { User } from '@app/models/User';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(users: User[], searchValue: string): User[] {
    if(!users || !searchValue){
      return users;
    }
    return users.filter(users=>
      users.ten?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())||
      users.tenbophan?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())||
      users.tendangnhap?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())||
      users.email?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())||
      users.sdt?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
      );
  }

}

