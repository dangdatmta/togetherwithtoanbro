import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BoPhan } from '@app/models/BoPhan';
import { ConfigReport } from '@app/models/ConfigReport';
import { AuthService } from '@modules/auth/services';
import { SBRouteData, SideNavItem } from '@modules/navigation/models';
import { NavigationService } from '@modules/navigation/services';
// import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'sb-side-nav-item',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './side-nav-item.component.html',
    styleUrls: ['side-nav-item.component.scss']
})
export class SideNavItemComponent implements OnInit {
    @Input() bophanid!: string;
    @Input() sideNavItem!: SideNavItem;
    @Input() isActive!: boolean;
    expanded = false;
    routeData!: SBRouteData;
    bophan: BoPhan[] = []
    bophanMenu: BoPhan[] = []
    currentUrl?: string
    id?: number
    Tests?: ConfigReport[]
    constructor(private authService: AuthService, private serverHttp: NavigationService, private router: Router) { }
    roleDetail: string[] = []
    roleName: string[] = []
    ngOnInit() {
        this.roleDetail = this.authService.getRoleDetail()
        this.roleName = this.authService.getRoleName()
        this.getALLBoPhan()
        this.getALLBoPhanForSubMenu()
    }

    getALLBoPhan() {
        this.serverHttp.getBoPhans().subscribe((data) => {
            this.bophan = data
        });
    }
    indexer: number = 1;
    index(){
        this.indexer += 1;
    }
    getALLBoPhanForSubMenu() {
        this.currentUrl = this.router.url
        this.serverHttp.getBoPhansForSubmenu().subscribe((data) => {
            data.forEach(element => {
                element.link= this.currentUrl
            });
            this.bophanMenu = data
            console.log('bophan',this.bophanMenu)
        });
    }

    getCauHinhByBoPhanID(bophanID: number){
        this.serverHttp.getCauHinhByBoPhanID(bophanID).subscribe((data) => {
            this.Tests = data
            console.log('cauhinhbybophan',data)
        });
    }
    idBoPhan?:number
    getIdMenu(submenuItem: any){
        this.idBoPhan = submenuItem.id
        this.bophanid = submenuItem.id
        this.authService.setIdBoPhan(this.bophanid)
        console.log('event',submenuItem.id)
    }

    idMenulv2?:number
    getIdMenuLv2(submenuItemLv2: ConfigReport){
        this.idMenulv2 = submenuItemLv2.id
        console.log('idlv2', this.idMenulv2)
    }
}
