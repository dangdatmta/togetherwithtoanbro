import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, ChildActivationEnd, Router } from '@angular/router';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { filter, catchError } from 'rxjs/operators';

import { SBRouteData } from '../models';

@Injectable()
export class NavigationService {
    _sideNavVisible$ = new BehaviorSubject(true);
    _routeData$ = new BehaviorSubject({} as SBRouteData);
    _currentURL$ = new BehaviorSubject('');
    private REST_API_SERVER = "https://localhost:44363/api"
    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            //Authorization: 'my-auth-token'
        })
    };
    constructor(public route: ActivatedRoute, public router: Router, private httpClient: HttpClient) {
        this.router.events
            .pipe(filter(event => event instanceof ChildActivationEnd))
            .subscribe(event => {
                let snapshot = (event as ChildActivationEnd).snapshot;
                while (snapshot.firstChild !== null) {
                    snapshot = snapshot.firstChild;
                }
                this._routeData$.next(snapshot.data as SBRouteData);
                this._currentURL$.next(router.url);
            });
    }

    sideNavVisible$(): Observable<boolean> {
        return this._sideNavVisible$;
    }

    toggleSideNav(visibility?: boolean) {
        if (typeof visibility !== 'undefined') {
            this._sideNavVisible$.next(visibility);
        } else {
            this._sideNavVisible$.next(!this._sideNavVisible$.value);
        }
    }

    routeData$(): Observable<SBRouteData> {
        return this._routeData$;
    }

    currentURL$(): Observable<string> {
        return this._currentURL$;
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong.
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // Return an observable with a user-facing error message.
        return throwError(
            'Something bad happened; please try again later.');
    }
    
    //quản lý bộ phận
    public getBoPhans(): Observable<any> {
        const url = `${this.REST_API_SERVER}/bophan`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getBoPhansForSubmenu(): Observable<any> {
        const url = `${this.REST_API_SERVER}/bophan/submenu`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getCauHinhByBoPhanID(bophanID: number): Observable<any> {
        const url = `${this.REST_API_SERVER}/cauhinh/`+bophanID;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
}
