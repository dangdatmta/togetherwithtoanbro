import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ConfigReport } from '@app/models/ConfigReport';
import { AuthService } from '@modules/auth/services';
declare var tableau: any;
@Component({
  selector: 'sb-embedded-iframe',
  templateUrl: './embedded-iframe.component.html',
  styleUrls: ['./embedded-iframe.component.scss']
})
export class EmbeddedIframeComponent implements AfterViewInit {

  constructor(private serverHttp: AuthService,) { }
  viz: any;
  @ViewChild("vizContainer") containerDiv?: ElementRef;

  ngAfterViewInit() {
    this.initTableau();
  }

  initTableau() {
    // const containerDiv = document.getElementById("vizContainer");
    this.serverHttp.getConfigReports().subscribe((data) => {
      console.log('chbc', data)
      
      const vizUrl = data[data.length-1].tableurl_id
      const options = {
        hideTabs: false,
        onFirstInteractive: () => {
          console.log("onFirstInteractive");
        },
        onFirstVizSizeKnown: () => {
          console.log("onFirstVizSizeKnown");
        }
      };
      this.viz = new tableau.Viz(
        this.containerDiv?.nativeElement,
        vizUrl,
        options
      );
    });

  }
}
