import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriMausacComponent } from './quan-tri-mausac.component';

describe('QuanTriMausacComponent', () => {
  let component: QuanTriMausacComponent;
  let fixture: ComponentFixture<QuanTriMausacComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriMausacComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriMausacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
