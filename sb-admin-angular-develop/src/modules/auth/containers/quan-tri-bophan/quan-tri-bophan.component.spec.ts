import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriBophanComponent } from './quan-tri-bophan.component';

describe('QuanTriBophanComponent', () => {
  let component: QuanTriBophanComponent;
  let fixture: ComponentFixture<QuanTriBophanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriBophanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriBophanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
