import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BoPhan } from '@app/models/BoPhan';
import { NhomLoaiDuLieu } from '@app/models/NhomLoaiDuLieu';
import { Role } from '@app/models/Role';
import { AuthService } from '@modules/auth/services';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
@Component({
  selector: 'sb-quan-tri-nhomloaidulieu',
  templateUrl: './quan-tri-nhomloaidulieu.component.html',
  styleUrls: ['./quan-tri-nhomloaidulieu.component.scss'],
  providers: [MessageService, ConfirmationService]
})

export class QuanTriNhomLoaiDuLieuComponent implements OnInit {
  editDialog?: boolean;
  submitted?: boolean;
  NLDLs: NhomLoaiDuLieu[] = []
  BoPhans?: BoPhan[]
  nldl?: NhomLoaiDuLieu
  constructor(
    private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private http: HttpClient
  ) { }
  private httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        //Authorization: 'my-auth-token'
    })
};
  ngOnInit(): void {
    this.getNLDLs()
    this.getBaoCaos()
    // this.roleSelected = Role.length
  }
  getNLDLs() {
    this.serverHttp.getNLDL().subscribe((data) => {
      this.NLDLs = data;
    });
  }
  getBaoCaos() {
    this.serverHttp.getBoPhans().subscribe((data) => {
      this.BoPhans = data;
    });
  }
  edit(nldl: NhomLoaiDuLieu) {
    this.nldl = { ...nldl };
    this.editDialog = true;
  }
  openNew() {
    this.nldl = {};
    this.submitted = false;
    this.editDialog = true;
  }
  hideDialog() {
    this.editDialog = false;
    this.submitted = false;
  }
  @ViewChild('fileInput') fileInput
  save() {
    this.submitted = true;
    if (this.nldl?.id) {
      this.NLDLs[this.findIndexById(this.nldl.id)] = this.nldl;
      this.serverHttp.updateNLDL(this.nldl.id, this.nldl).subscribe((data) => { }
      );
      this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
      this.nldl = {};
    }
    else {
      const filedata = new FormData()
      filedata.append('upload', this.fileInput.nativeElement.files[0])
      this.serverHttp.addNLDL(filedata,this.nldl as NhomLoaiDuLieu).subscribe((data) => {
        console.log('dâzta', data)
        this.getNLDLs()
      });
      this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Tạo nhóm loại dữ liệu thành công', life: 3000 });
      this.nldl = {};
      this.getNLDLs()
    }
    this.NLDLs = [...this.NLDLs];
    this.editDialog = false;

  }

  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.NLDLs.length; i++) {
      if (this.NLDLs[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(nldl: NhomLoaiDuLieu) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa NLDL ' + nldl.name + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {

        this.serverHttp.deleteNLDL(nldl.id as number).subscribe((data) => { }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.NLDLs = this.NLDLs.filter(val => val.id !== nldl.id);
        this.nldl = {};
        this.messageService.add({ key: "br", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}
