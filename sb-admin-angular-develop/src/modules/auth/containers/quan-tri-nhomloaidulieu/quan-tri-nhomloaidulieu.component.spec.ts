import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriNhomloaidulieuComponent } from './quan-tri-nhomloaidulieu.component';

describe('QuanTriNhomloaidulieuComponent', () => {
  let component: QuanTriNhomloaidulieuComponent;
  let fixture: ComponentFixture<QuanTriNhomloaidulieuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriNhomloaidulieuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriNhomloaidulieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
