import { AfterViewInit, Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TinhThanh } from '@app/models/TinhThanh';
import { AuthService } from '@modules/auth/services';

@Component({
  selector: 'sb-quan-tri-tinhthanh',
  templateUrl: './quan-tri-tinhthanh.component.html',
  styleUrls: ['./quan-tri-tinhthanh.component.scss'],
})
export class QuanTriTinhthanhComponent implements OnInit {
  searchValue?: string
  EditDialog?: boolean
  submitted?: boolean
  id?: string
  TinhThanhs?: TinhThanh[] = []
  tinhthanh?: TinhThanh
  statuses?: any[];
  constructor(
    public sanitizer: DomSanitizer,
    private serverHttp: AuthService
  ) { this.sanitizer = sanitizer; }

  ngOnInit(): void {
    this.loadAllTinhThanh();
  }

  loadAllTinhThanh() {
    this.serverHttp.getTinhThanhs().subscribe((data) => {
      console.log('data', data);
      this.TinhThanhs = data;
    });
  }
  edit(tinhthanh: TinhThanh) {
    this.tinhthanh = { ...tinhthanh };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
}

