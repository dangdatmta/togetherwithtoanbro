import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriTinhthanhComponent } from './quan-tri-tinhthanh.component';

describe('QuanTriTinhthanhComponent', () => {
  let component: QuanTriTinhthanhComponent;
  let fixture: ComponentFixture<QuanTriTinhthanhComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriTinhthanhComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriTinhthanhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
