import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { F_CSKH05 } from '@app/models/F_CSKH05';
import { NhomLoaiDuLieu } from '@app/models/NhomLoaiDuLieu';
import { TCKT01 } from '@app/models/TCKT.01';
import { AuthService } from '@modules/auth/services';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver/dist/FileSaver';
import { ValidateTCKT01 } from '@app/models/ValidateTCKT01';
import { StringMapWithRename } from '@angular/compiler/src/compiler_facade_interface';
@Component({
    selector: 'sb-static',
    templateUrl: './static.component.html',
    styleUrls: ['static.component.scss'],
    providers: [MessageService, ConfirmationService]
})
export class StaticComponent implements OnInit {
    @ViewChild('fileInput') fileInput
    validateDialog?: boolean;
    submitted?: boolean;
    versions: number[] = []
    public isCollapsed = false
    message?: string;
    timeUpdate?: Date;
    userCreate?: string;
    TCKT01: TCKT01[] = []
    f_cskh05?: F_CSKH05
    countRecords?: number
    version?: number
    maxSTT?: number

    NLDL?: NhomLoaiDuLieu[]
    Year?: number[]
    KyBC?: string[]
    alertNoData?: string
    validateTCKT01s?: ValidateTCKT01[]
    constructor(
        private serverHttp: AuthService,
        private messageService: MessageService,
        private confirmService: ConfirmationService
    ) {}
    bophan_id?: number
    ngOnInit(): void {
        //this.getAllVersion()
        
        this.getYears()
       
        this.getNLDLByBoPhanID()
    }
    // getkybcidtest(idsss: number){
    //     console.log('testid',idsss)
    // }
    getLoaiKys(year: number, nldl_id: number) {
        this.errorMessValidate = []
        this.validateTCKT01s = []
        this.serverHttp.getLoaiKys(year, nldl_id).subscribe((data) => {
            this.KyBC = data
            console.log('year', year)
            console.log('nldl_id', nldl_id)
        });
        this.KyBC = []
    }
    getYears() {
        this.serverHttp.getYears().subscribe((data) => {
            this.Year = data
        });
    }
    getNLDLByBoPhanID() {
        this.serverHttp.getNLDL().subscribe((data) => {
            this.NLDL = data
        });
    }
    refreshVersion() {
        this.versions.length = 0
    }
    header1?: string
    header2?: string
    getAllVersion(nldl_id: number, kybc_id: number) {
        this.errorMessValidate = []
        this.validateTCKT01s = []
        this.serverHttp.getListVersion(nldl_id, kybc_id).subscribe((data) => {
            console.log('version', data);
            this.versions = data;
            if (data != 0) {
                for (var i = this.versions.length - 1; i >= 0; i--) {
                    this.serverHttp.getTCKT01_ByVersion(this.versions[i], nldl_id, kybc_id).subscribe((data) => {
                        this.TCKT01.push(data)
                        this.countRecords = data.length
                        this.maxSTT = data[this.versions.length - 1].stt
                        this.header1 = data[this.versions.length - 1].header1
                        this.header2 = data[this.versions.length - 1].header2
                    });
                }
            } else {
                this.alertNoData = "Không tìm thấy dữ liệu phù hợp"
            }

        });
        console.log('KYBC', kybc_id)
        console.log('NDLID', nldl_id)
    }

    overrideVersion(version: number) {
        console.log('test')
        this.version = version;
        console.log('ver', this.version)
        this.confirmService.confirm({
            message: 'Dữ liệu sẽ bị thay đổi, bạn có chắc chắn muốn ghi đè?',
            header: 'Xác nhận',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.serverHttp.OverrideVersion(this.version).subscribe(data => {
                    // this.getAllVersion()
                });

                this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Ghi đè dữ liệu thành công', life: 3000 });
            }
        });
    }

    exportexcel(version: number) {
        this.version = version;
        /* pass here the table id */
        let element = document.getElementById(this.version.toString());
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        /* save to file */
        XLSX.writeFile(wb, 'excelFile.xlsx');
    }

    uploadFile(nldl_id: number, kybc_id: number) {
        let formData = new FormData();
        formData.append('upload', this.fileInput.nativeElement.files[0])
        this.confirmService.confirm({
            message: 'Bạn có thật sự muốn thêm dữ liệu không?',
            header: 'Xác nhận',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                if(this.errorMessValidate == ""){
                    this.serverHttp.UploadExcel(formData, nldl_id, kybc_id).subscribe(result => {
                        if (result == "Excel file has been successfully uploaded") {
                            this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Tải file thành công', life: 3000 });
                        }
                        else{
                            this.messageService.add({ key: 'br', severity: 'error', summary: 'Thất bại', detail: 'Dữ liệu bạn chọn còn lỗi hoặc định dạng file chưa đúng!!!', life: 3000 });
                        }
                    });
                }
                else{
                    this.messageService.add({ key: 'br', severity: 'error', summary: 'Thất bại', detail: 'Dữ liệu bạn chọn còn lỗi hoặc định dạng file chưa đúng!!!', life: 3000 });
                }
            }
        });
    }
    errorMessValidate?: any
    validateExcel() {
        this.errorMessValidate = []
        this.validateTCKT01s = []
        let formData = new FormData();
        formData.append('upload', this.fileInput.nativeElement.files[0])
        this.serverHttp.validate(formData).subscribe(result => {
            if (result == "") {
                this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Dữ liệu của file không có lỗi', life: 3000 });
            }
            if (result == "Định dạng file không được hỗ trợ!!!") {
                this.messageService.add({ key: 'br', severity: 'error', summary: 'Thất bại', detail: 'Định dạng này không được hỗ trợ!!!', life: 3000 });
            }

            else {

                this.errorMessValidate = result;

                this.serverHttp.getValidateTCKT01().subscribe(data => {
                    console.log('vali', data)
                    this.validateTCKT01s = data
                });
            }
        });
    }
    downloadExcel(nldl_id: number){
        this.serverHttp.DownloadExcel(nldl_id).subscribe((data) => {
            var file = new Blob([data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'})
            this.serverHttp.getNameFileToDownload(nldl_id).subscribe((data) => {saveAs(file, data)})
            this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Tải file mẫu thành công', life: 3000 });
        });
    }
    // getValidateTCKT01(){

    // }
    edit() {
        this.validateDialog = true;
    }
    hideDialog() {
        this.validateDialog = false;
        this.submitted = false;
    }
    openNew() {
        this.submitted = false;
        this.validateDialog = true;
    }
    // fileName = '...';
    // downloadFile() {
    //     // check something
    //     // ...

    //     // download file
    //     this.serverHttp.download().subscribe(
    //         res => {
    //             const blob = new Blob([res.blob()], { type : 'application/vnd.ms.excel' });
    //             const file = new File([blob], this.fileName + '.xlsx', { type: 'application/vnd.ms.excel' });
    //             saveAs(file);
    //         },
    //         res => {
    //             // notify error
    //         }
    //     );
    // }
    title = 'ThachBanCli';
}
