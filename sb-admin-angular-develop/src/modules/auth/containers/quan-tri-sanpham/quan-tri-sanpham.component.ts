import { Component, NgModule, OnInit } from '@angular/core';

import { SanPham } from '@app/models/SanPham';
import { AuthService } from '@modules/auth/services';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-sanpham',
  templateUrl: './quan-tri-sanpham.component.html',
  styleUrls: ['./quan-tri-sanpham.component.scss']
})
export class QuanTriSanphamComponent implements OnInit {



 
  SanPham: SanPham[]=[]
  sanPhamDetail: string[] = []
  sanPham?: SanPham
  EditDialog?: boolean
  sanPhamSelected?:boolean
  productDialog?: boolean
  submitted?: boolean
  text?: string

  results?: string[];

 

  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService) { }

  ngOnInit(): void {
    this.getAllSanPham()
  }
 

  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
 getAllSanPham() {
    this.serverHttp.getSanPham().subscribe((data) => {
      console.log('data', data);
      this.SanPham = data;
    });
  }
  getSanPhamId(id) {
    this.EditDialog = true;
    this.serverHttp.getSanPhamID(id).subscribe((data) => {
      console.log('data', data);
    });
  }
  openNew() {
    this.sanPham = {};
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(sanPham:  SanPham) {
    this.sanPham = { ...sanPham };
    this.EditDialog = true;
  }

  save() {
    this.submitted = false;
    if (this.sanPham?.id) {
      this.SanPham[this.findIndexById(this.sanPham.id)] = this.sanPham;
      this.serverHttp.updateSanPham(this.sanPham.id, this.sanPham).subscribe((data) => { }
      );
      this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
      this.sanPham = {};
      //this.getAllRole()
    }
    else {
      //this.product.id = this.createId();
      this.serverHttp.addSanPham(this.sanPham as SanPham).subscribe((data) => { console.log('data', data) });
      this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Thêm mới thành công', life: 3000 });
      this.sanPham = {};
      this.getAllSanPham()
    }
    this.SanPham = [...this.SanPham];
    this.EditDialog = false;
    
  }
  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.SanPham.length; i++) {
      if (this.SanPham[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(sanPham: SanPham) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + sanPham.ten + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteColor(sanPham.id as any).subscribe((data) => { console.log('data', data) }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.SanPham = this.SanPham.filter(val => val.id !== sanPham.id);
        this.sanPham = {};
        this.messageService.add({ key: "br", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}
