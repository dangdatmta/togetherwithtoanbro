import { Component, NgModule, OnInit } from '@angular/core';
import { Role } from '@app/models/Role';
import { User } from '@app/models/User';
import { NhomNSD } from '@app/models/NhomNSD';
import { UserRole } from '@app/models/UserRole';
import { AuthService } from '@modules/auth/services';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-nhomnguoisudung',
  templateUrl: './quan-tri-nhomnguoisudung.component.html',
  styleUrls: ['./quan-tri-nhomnguoisudung.component.scss']
})
export class QuanTriNhomnguoisudungComponent implements OnInit {

  boPhanDetail: string[] = []
  chucNangDetail: string[] = []
  nhomNSDDetail: string[] = []
  roleName: string[] = []
  FullName?: string
  roles: UserRole[] = []
  titleDialog?: boolean
  roleSelected?: number
  User: User[] = []
  Role: Role[] = []
  NhomNSD: NhomNSD[]=[]
  role?: Role
  userRoles: UserRole[] = []
  user?: User
  nhomNSD?: NhomNSD
  EditDialog?: boolean
  productDialog?: boolean
  submitted?: boolean
  file0?: NhomNSD[];
  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    ) { }
  ngOnInit(): void {
   // this.serverHttp.getRoleName().then(role => this.files3 = role);
    this.FullName = this.serverHttp.getUserName()
    this.boPhanDetail = this.serverHttp.getBoPhanDetail()
   // this.groupUserDetail = this.serverHttp.getgroupUserDetail()

    this.roleName = this.serverHttp.getBoPhanName()
    console.log('length',this.roleName.length)
    this.getAllUser()
    // this.getAllRole()
    this.getAllNhomNSD()
    this.roleSelected = this.Role.length
    this.roles = new Array<UserRole>();
  
    //////////////////////////////////////
    
this.file0 = [
//   {
//   "label": "Quản trị",
//   "data": "Documents Folder",
//   "expandedIcon": "fa fa-folder-open",
//   "collapsedIcon": "fa fa-folder",
//   "children": [{
//   "label": "Master Data",
//   "data": "Work Folder",
//   "expandedIcon": "fa fa-folder-open",
//   "collapsedIcon": "fa fa-folder",
//   "children": [{ "label": "Đại lý", "icon": "fa fa-file-word-o", "data": "Expenses Document" },
//    { "label": "Vùng miền", "icon": "fa fa-file-word-o", "data": "Resume Document" }]
//   },
//   {
//   "label": "Nhóm nghiệp vụ",
//   "data": "Home Folder",
//   "expandedIcon": "fa fa-folder-open",
//   "collapsedIcon": "fa fa-folder",
//   "children": [{ "label": "Sửa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" },
//   { "label": "Xóa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" }]
//   },
//   {
//     "label": "Quản trị nhập dữ liệu",
//     "data": "Home Folder",
//     "expandedIcon": "fa fa-folder-open",
//     "collapsedIcon": "fa fa-folder",
//     "children": [{ "label": "Nhóm loại dữ liệu", "icon": "fa fa-file-word-o", "data": "Invoices for this month",
//   "expandedIcon": "fa fa-folder-open",
//       "collapsedIcon": "fa fa-folder",
//       "children": [{ "label": "Sửa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" },
//       { "label": "Xóa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" }]
//        },
//     { "label": "Quản trị kỳ báo cáo", "icon": "fa fa-file-word-o", "data": "Invoices for this month",
//     "expandedIcon": "fa fa-folder-open",
//     "collapsedIcon": "fa fa-folder",
//     "children": [{ "label": "Sửa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" },
//     { "label": "Xóa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" }]
//      }]
//     },
//     {
//       "label": "Quản trị người dùng",
//       "data": "Home Folder",
//       "expandedIcon": "fa fa-folder-open",
//       "collapsedIcon": "fa fa-folder",
//       "children": [{ "label": "Quản trị phân quyền", "icon": "fa fa-file-word-o", "data": "Invoices for this month",
//       "expandedIcon": "fa fa-folder-open",
//       "collapsedIcon": "fa fa-folder",
//       "children": [{ "label": "Sửa ", "icon": "fa fa-file-word-o", "data": "Invoices for this month" },
//       { "label": "Xóa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" }]
//        },
//       { "label": "Tạo user và phân quyền", "icon": "fa fa-file-word-o", "data": "Invoices for this month",
//       "expandedIcon": "fa fa-folder-open",
//       "collapsedIcon": "fa fa-folder",
//       "children": [{ "label": "Sửa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" },
//       { "label": "Xóa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" }]
//        }]
      
//     },
//       {
//         "label": "Quản trị báo cáo",
//         "data": "Home Folder",
//         "expandedIcon": "fa fa-folder-open",
//         "collapsedIcon": "fa fa-folder",
//         "children": [{ "label": "Cấu hình báo cáo", "icon": "fa fa-file-word-o", "data": "Invoices for this month",
//         "expandedIcon": "fa fa-folder-open",
//         "collapsedIcon": "fa fa-folder",
//         "children": [{ "label": "Sửa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" },
//         { "label": "Xóa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" }]
//          },
//         { "label": "Nhóm view báo cáo", "icon": "fa fa-file-word-o", "data": "Invoices for this month",
//         "expandedIcon": "fa fa-folder-open",
//         "collapsedIcon": "fa fa-folder",
//         "children": [{ "label": "Sửa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" },
//         { "label": "Xóa", "icon": "fa fa-file-word-o", "data": "Invoices for this month" }]
//          }]
//         },



// ]
//   },
//   {
//   "label": "Báo cáo",
//  // "data": "Documents Folder",
//   "expandedIcon": "fa fa-folder-open",
//   "collapsedIcon": "fa fa-folder",
//   "children": [
//   { "label": "Xem báo cáo", "icon": "fa fa-file-image-o", "data": "1" },]
//   },
//   {
//   "label": "Nhập dữ liệu",
// // "data": "Documents Folder",
//   "expandedIcon": "fa fa-folder-open",
//   "collapsedIcon": "fa fa-folder",
//   "children": [{
//   "label": "Upload dữ liệu",
//   "data": "2",
//   },
// ]
//   }
  ];
    ////////////////////////////////////
    // call hàm getFiles
 //   this.nodeService.getFiles().then(files => this.files = files);
  }
  //  hàm lấy data tree checkbox
//   getFiles() {
//     return this.http.get('showcase/resources/data/files.json')
//                 .toPromise()
//                 .then(res => <TreeNode[]> res.json().data);
// }
  getAllUser() {
    this.serverHttp.getUsers().subscribe((data) => {
      console.log('data', data);
      this.User = data;
    });
  }
  // getAllRole() {
  //   this.serverHttp.getRole().subscribe((data) => {
  //     console.log('role', data);
  //     this.Role = data;
  //   });
  // }
 
  getAllNhomNSD() {
    this.serverHttp.getNhomNSD().subscribe((data) => {
      console.log('data', data);
      this.NhomNSD = data;
    });
  }
 
  openNew() {
    this.nhomNSD = {};
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(nhomNSD:  NhomNSD) {
    this.nhomNSD = { ...nhomNSD };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }

  save() {
    this.submitted = true;
  
    // this.user = { ...this.user, UserRoles: this.userRoles };
    this.nhomNSD ={...this.nhomNSD};
    this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Tạo nhóm thành công', life: 3000 });
    // console.log('x', this.user)
    // if (this.user?.Id) {
    //   this.User[this.findIndexById(this.user.Id)] = this.user;
    //   //this.role.IsActive = this.roleSelected
    //   // this.serverHttp.updateUser(this.role.Id, this.role).subscribe((data) => { console.log('data', data) }
    //   // );
    //   // this.getAllRole();
    //   // this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
    // }
    // else {
    //this.product.id = this.createId();

    this.serverHttp.addNhomNSD(this.nhomNSD as NhomNSD).subscribe((data) => { console.log('NhomNSD', data) });
    this.getAllNhomNSD()

    // }
    // this.User = [...this.User];
    this.nhomNSD = {};
    this.EditDialog = false;
  }

  // findIndexById(id: string): number {
  //   let index = -1;
  //   for (let i = 0; i < this.User.length; i++) {
  //     if (this.User[i].Id === id) {
  //       index = i;
  //       break;
  //     }
  //   }
  //   return index;
  // }
  getNhomNSDId(id) {
    this.EditDialog = true;
    this.serverHttp.getNhomNSDID(id).subscribe((data) => {
      console.log('data', data);
    });
  }

  delete(nhomNSD: NhomNSD) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + nhomNSD.ten + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteNhomNSD(nhomNSD.id as any).subscribe((data) => { console.log('data', data) }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.NhomNSD = this.NhomNSD.filter(val => val.id !== nhomNSD.id);
        this.nhomNSD = {};
        this.messageService.add({ key: "br", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}



