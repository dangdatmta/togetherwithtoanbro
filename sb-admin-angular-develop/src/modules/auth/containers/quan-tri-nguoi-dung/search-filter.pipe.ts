import { Pipe, PipeTransform } from '@angular/core';
import { Region } from '@app/models/Region';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(regions: Region[], searchValue: string): Region[] {
    if(!regions || !searchValue){
      return regions;
    }
    return regions.filter(regions=>
      regions.ten?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
      );
  }

}
