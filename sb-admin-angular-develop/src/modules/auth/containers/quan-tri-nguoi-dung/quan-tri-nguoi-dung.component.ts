import { Component, NgModule, OnInit } from '@angular/core';
import { BoPhan } from '@app/models/BoPhan';
import { User } from '@app/models/User';
import { UserRole } from '@app/models/UserRole';
import {User_NhomNSD} from '@app/models/User_NhomNSD';
import {NhomNSD} from '@app/models/NhomNSD';
import {User_BoPhan} from '@app/models/User_BoPhan';
import { AuthService } from '@modules/auth/services';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { Region } from '@app/models/Region';
import { TinhThanh } from '@app/models/TinhThanh';
import { SanPham } from '@app/models/SanPham';
import { DaiLy } from '@app/models/DaiLy';
import { Color } from '@app/models/Color';

@Component({
  selector: 'sb-quan-tri-nguoi-dung',
  templateUrl: './quan-tri-nguoi-dung.component.html',
  styleUrls: ['./quan-tri-nguoi-dung.component.scss']
})
export class QuanTriNguoiDungComponent implements OnInit {
  isCheck?: boolean
  boPhanDetail: string[] = []
  nhomNSDDetail: string[]=[]
  boPhanName: string[] = []
  nhomNSDName:string[]=[]
  FullName?: string
  roles: UserRole[] = []
  userBoPhan: User_BoPhan[]=[]
  userNhomNSD: User_NhomNSD[]=[]
  titleDialog?: boolean
  boPhanSelected?: number
  nhomNSDSelected?: number
  User: User[] = []
  BoPhan: BoPhan[] = []
  NhomNSD: NhomNSD[]=[]
  nhomNSD? :NhomNSD
  boPhan?: BoPhan
  userRoles: UserRole[] = []
  userBoPhans: User_BoPhan[]=[]
  userNhomNSDs: User_NhomNSD[]=[]
  

  user?: User
  EditDialog?: boolean
  productDialog?: boolean
  submitted?: boolean
  EditDialog2?: boolean

  KhuVucs?: Region[]
  TinhThanhs?: TinhThanh[]
  DaiLys?: DaiLy[]
  SanPhams?: SanPham[]
  MauSacs?: Color[]

  //update
  

  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService) { }
  ngOnInit(): void {
    this.FullName = this.serverHttp.getUserName()
    this.boPhanDetail = this.serverHttp.getBoPhanDetail()
    this.nhomNSDDetail=this.serverHttp.getNhomNSDDetail()
    this.boPhanName = this.serverHttp.getBoPhanName()
    this.nhomNSDName=this.serverHttp.getNhomNSDName()
    console.log('length', this.boPhanName.length)
    this.getAllUser()
    this.getAllBoPhan()
    this.getAllNhomNSD()
    this.boPhanSelected = this.BoPhan.length
    this.nhomNSDSelected=this.NhomNSD.length
    this.roles = new Array<UserRole>();
    this.userNhomNSD=new Array<User_NhomNSD>();
    this.userBoPhan=new  Array<User_BoPhan>();
    this.getDaiLys()
    this.getSanPhams()
    this.getMauSacs()
    this.getKhuVucs()
    this.getTinhThanhs()
  }
  getAllUser() {
    this.serverHttp.getUsers().subscribe((data) => {
      console.log('data', data);
      this.User = data;

      // this.User.forEach(element => {
      //   if (element.UserRoles != null) {
      //     element.UserRoles?.forEach(element => {
      //       if (element.Id != null) {
      //         this.isCheck = true
      //       } else {
      //         this.isCheck = false
      //       }
      //     });
      //   }
      //   else{
      //     this.isCheck = false
      //   }
      // });

      // this.User.forEach(element => {
      //   if (element.UserRoles != null) {
      //     element.UserRoles?.forEach(element => {
      //       if (element.Id != null) {
      //         this.isCheck = true
      //       } else {
      //         this.isCheck = false
      //       }
      //     });
      //   }
      //   else{
      //     this.isCheck = false
      //   }
      // });
    });
  }
  getAllBoPhan() {
    this.serverHttp.getBoPhans().subscribe((data) => {
      console.log('Bộ phận', data);
      this.BoPhan = data;
    });
  }
  getAllNhomNSD() {
    this.serverHttp.getNhomNSD().subscribe((data) => {
      console.log('nhóm người sử dụng', data);
      this.NhomNSD = data;
    });
  }

  openNew() {
    this.user = {};
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(user: User) {
    this.user = { ...user };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
  edit2(user: User) {
    this.user = { ...user };
    this.EditDialog2 = true;
  }
  hideDialog2() {
    this.EditDialog2 = false;
    this.submitted = false;
  }
  save() {
    this.submitted = true;
    if (this.user?.id) {
      this.user = { ...this.user, User_BoPhans:this.userBoPhans,User_NhomNSDs:this.userNhomNSDs };
      this.User[this.findIndexById(this.user.id)] = this.user;
      this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
      this.serverHttp.updateUser(this.user.id, this.user).subscribe((data) => { console.log('data', data) }
      );
      
      
    }
    else {
      debugger
      this.user = { ...this.user, User_BoPhans:this.userBoPhans,User_NhomNSDs:this.userNhomNSDs };
      this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Tạo nhân viên thành công', life: 3000 });
      this.serverHttp.addUser(this.user as User).subscribe((data) => { console.log('Users', data) });
      this.User.push(this.user)
    }
    
    this.User = [...this.User];
    this.user = {}
    this.userRoles = []
    this.userBoPhans = []
    this.userNhomNSDs = []

    this.EditDialog = false;
    
    
  }


  findIndexById(id?: number): number {
    let index = -1;
    for (let i = 0; i < this.User.length; i++) {
      if (this.User[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }
  getBoPhanId(e: any, id: number) {
    if (e.target.checked) {
      this.userBoPhans.push(new User_BoPhan(id));
    } else {
      console.log(id + 'uncheckedBoPhan')
      this.userBoPhans = this.userBoPhans.filter(m => m.BoPhan_id != id)
    }
    console.log(this.userBoPhans)
  }
  getNhomNSDId(e: any, id: number) {
    if (e.target.checked) {
      this.userNhomNSDs.push(new User_NhomNSD(id));
    } else {
      console.log(id + 'uncheckedNNSD')
      this.userNhomNSDs = this.userNhomNSDs.filter(m => m.NhomNSD_id != id)
    }
    console.log(this.userNhomNSDs)
  }

  delete(user: User) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + user.ten + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteUser(user.id as number).subscribe((data) => { console.log('data', data) }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.User = this.User.filter(val => val.id !== user.id);
        this.user = {};
        this.messageService.add({ key: "br", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }

  //
  getKhuVucs(){
    this.serverHttp.getRegion().subscribe((data) => { this.KhuVucs = data });
  }
  getTinhThanhs(){
    this.serverHttp.getTinhThanhs().subscribe((data) => { this.TinhThanhs = data });
  }
  getDaiLys(){
    this.serverHttp.getDaiLys().subscribe((data) => { this.DaiLys = data });
  }
  getSanPhams(){
    this.serverHttp.getSanPham().subscribe((data) => { this.SanPhams = data });
  }
  getMauSacs(){
    this.serverHttp.getColor().subscribe((data) => { this.MauSacs = data });
  }
}
