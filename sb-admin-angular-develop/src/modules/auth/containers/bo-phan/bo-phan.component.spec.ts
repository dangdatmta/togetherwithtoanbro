import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoPhanComponent } from './bo-phan.component';

describe('BoPhanComponent', () => {
  let component: BoPhanComponent;
  let fixture: ComponentFixture<BoPhanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoPhanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoPhanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
