import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriNhomQuyenComponent } from './quan-tri-nhom-quyen.component';

describe('QuanTriNhomQuyenComponent', () => {
  let component: QuanTriNhomQuyenComponent;
  let fixture: ComponentFixture<QuanTriNhomQuyenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriNhomQuyenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriNhomQuyenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
