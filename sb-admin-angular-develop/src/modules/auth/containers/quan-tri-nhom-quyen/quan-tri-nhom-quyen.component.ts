import { Component, OnInit } from '@angular/core';
import { Role } from '@app/models/Role';
import { AuthService } from '@modules/auth/services';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
@Component({
  selector: 'sb-quan-tri-nhom-quyen',
  templateUrl: './quan-tri-nhom-quyen.component.html',
  styleUrls: ['./quan-tri-nhom-quyen.component.scss'],
  providers: [MessageService, ConfirmationService]
})

export class QuanTriNhomQuyenComponent implements OnInit {
  roleName: string[] = [] 
  roleSelected?: boolean
  Role: Role[] = []
  role?: Role
  editDialog?: boolean;
  submitted?: boolean;
  constructor(
    private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService
  ) { }
  ngOnInit(): void {
    this.roleName = this.serverHttp.getRoleName()
    this.getAllRole()
    // this.roleSelected = Role.length
  }
  getAllRole() {
    this.serverHttp.getRole().subscribe((data) => {
      console.log('data', data);
      this.Role = data;
    });
  }
  edit(role: Role) {
    this.role = { ...role };
    this.editDialog = true;
  }
  openNew() {
    this.role = {};
    this.submitted = false;
    this.editDialog = true;
  }
  hideDialog() {
    this.editDialog = false;
    this.submitted = false;
  }

  save() {
    this.submitted = true;
    if (this.role?.Id) {
      this.Role[this.findIndexById(this.role.Id)] = this.role;
      this.role.IsActive = this.roleSelected
      this.serverHttp.updateRole(this.role.Id, this.role).subscribe((data) => { console.log('data', data) }
      );
      //this.getAllRole();
      
      this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
      this.role = {};
      //this.getAllRole()
    }
    else {
      //this.product.id = this.createId();
      this.serverHttp.addRole(this.role as Role).subscribe((data) => { console.log('data', data) });
      this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Tạo quyền thành công', life: 3000 });
      this.role = {};
      this.getAllRole()
    }
    this.Role = [...this.Role];
    this.editDialog = false;
    
  }

  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.Role.length; i++) {
      if (this.Role[i].Id === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  delete(role: Role) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa quyền ' + role.Name + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {

        this.serverHttp.deleteRole(role.Id as number).subscribe((data) => { console.log('data', data) }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.Role = this.Role.filter(val => val.Id !== role.Id);
        this.role = {};
        this.messageService.add({ key: "br", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}
