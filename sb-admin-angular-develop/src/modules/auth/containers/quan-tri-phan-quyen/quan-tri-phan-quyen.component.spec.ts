import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanTriPhanQuyenComponent } from './quan-tri-phan-quyen.component';

describe('QuanTriPhanQuyenComponent', () => {
  let component: QuanTriPhanQuyenComponent;
  let fixture: ComponentFixture<QuanTriPhanQuyenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanTriPhanQuyenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanTriPhanQuyenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
